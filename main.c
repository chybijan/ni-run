#include <stdio.h>
#include <malloc.h>

#include "src/astinterpreter/AstInterpreter.h"
#include "src/bcinterpreter/BcInterpreter.h"
#include "src/bccompiler/BcCompiler.h"
#include "reference/parser.h"

void runAstInterpreter(Ast * ast){
    astInterpret(ast);
}

void runBcInterpreter(unsigned char * bcProgram){
    bcInterpret(bcProgram);
}

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "Error: expected at least one argument\n");
		return 1;
	}

    char * command = argv[1];

    char * file = argv[argc-1];

    FILE *f = fopen(file, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    unsigned char *buf = malloc(fsize + 1);
    fread(buf, fsize, 1, f);
    fclose(f);

    if(strcmp(command, "ast_interpret") == 0){

        Arena arena;
        arena_init(&arena);

        Ast * ast = parse_src(&arena, (Str) { .str = buf, .len = fsize });

        if (ast == NULL) {
            fprintf(stderr, "Failed to parse source\n");
            arena_destroy(&arena);
            free(buf);
            return 1;
        }

        runAstInterpreter(ast);

        arena_destroy(&arena);

    }else if (strcmp(command, "bc_interpret") == 0){
        runBcInterpreter(buf);
    }else if(strcmp(command, "bc_compile") == 0){
        Arena arena;
        arena_init(&arena);

        Ast * ast = parse_src(&arena, (Str) { .str = buf, .len = fsize });

        if (ast == NULL) {
            fprintf(stderr, "Failed to parse source\n");
            arena_destroy(&arena);
            free(buf);
            return 1;
        }

        bcCompile(ast, stdout);

        arena_destroy(&arena);
    }else if( strcmp(command, "run") == 0){
        // Compile code to BC and run bc interpreter

        Arena arena;
        arena_init(&arena);

        Ast * ast = parse_src(&arena, (Str) { .str = buf, .len = fsize });

        if (ast == NULL) {
            fprintf(stderr, "Failed to parse source\n");
            arena_destroy(&arena);
            free(buf);
            return 1;
        }




        FILE * outBC = fopen("temp.bc", "wb");

        bcCompile(ast, outBC);

        fclose(outBC);

        FILE * bcIn= fopen("temp.bc", "rb");
        fseek(bcIn, 0, SEEK_END);
        long bcInsize = ftell(bcIn);
        fseek(bcIn, 0, SEEK_SET);  /* same as rewind(f); */

        unsigned char *bcBuff = malloc(bcInsize + 1);
        fread(bcBuff, bcInsize, 1, bcIn);
        fclose(bcIn);

        runBcInterpreter(bcBuff);

        arena_destroy(&arena);

        free(bcBuff);

    }

    free(buf);
    return 0;

}
