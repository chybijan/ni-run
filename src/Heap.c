//
// Created by jerror on 3/8/23.
//

#include "Heap.h"

Heap * heap_alloc(size_t capacity){

    // allocate memory
    Heap * heap = (Heap *) malloc(sizeof(Heap));
    unsigned char * mem = (unsigned char *) malloc(capacity * sizeof(unsigned char));

    heap->mem = mem;
    heap->capacity = capacity;
    heap->pos = 0;

    return heap;

}

void heap_destroy(Heap * heap){
    // Deallocate memory
    free(heap->mem);
    free(heap);
}

void heap_align(Heap * heap, size_t alignment){
    heap->pos = ((heap->pos / alignment) + 1) * alignment;
}

RuntimeObject * convertPtrToRuntimeObject(Value valuePtr){
    return (RuntimeObject *) valuePtr.valueInHeap;
}

unsigned char * allocate(Heap * heap, size_t byteSize){
    unsigned char * allocatedMemoryBlock = heap->mem + heap->pos;
    heap->pos = heap->pos + byteSize;
    return allocatedMemoryBlock;
}

unsigned char * allocate_integer(i32 value, Heap * heap){
    heap_align(heap, 8);

    IntegerRuntimeObject * integerRuntimeObject = (IntegerRuntimeObject *) allocate(heap, sizeof(IntegerRuntimeObject));

    integerRuntimeObject->type = TYPE_INTEGER;
    integerRuntimeObject->value = value;

    return (unsigned char *) integerRuntimeObject;
}

unsigned char * allocate_boolean(bool value, Heap * heap){
    heap_align(heap, 8);

    BooleanRuntimeObject * booleanRuntimeObject = (BooleanRuntimeObject *) allocate(heap, sizeof(BooleanRuntimeObject));

    booleanRuntimeObject->type = TYPE_BOOLEAN;
    booleanRuntimeObject->value = value;

    return (unsigned char *) booleanRuntimeObject;
}

unsigned char * allocate_null(Heap * heap){
    heap_align(heap, 8);

    NullRuntimeValue * nullRuntimeValue = (NullRuntimeValue *) allocate(heap, sizeof(NullRuntimeValue));

    nullRuntimeValue->type = TYPE_NULL;
    return (unsigned char *) nullRuntimeValue;
}

unsigned char * allocate_ast_function(AstFunction * function, Heap * heap){
    heap_align(heap, 8);

    AstFunctionRuntimeValue * functionRuntimeValue = (AstFunctionRuntimeValue *) allocate(heap, sizeof(AstFunctionRuntimeValue));

    functionRuntimeValue->type = TYPE_FUNCTION;
    functionRuntimeValue->function = function;

    return (unsigned char *) functionRuntimeValue;
}

unsigned char * allocate_bc_function(size_t indexToConstPool, Heap * heap){
    heap_align(heap, 8);

    BcFunctionRuntimeValue * functionRuntimeValue = (BcFunctionRuntimeValue *) allocate(heap, sizeof(BcFunctionRuntimeValue));

    functionRuntimeValue->type = TYPE_FUNCTION;
    functionRuntimeValue->indexToConstPool = indexToConstPool;

    return (unsigned char *) functionRuntimeValue;
}

ArrayRuntimeObject * allocate_array(Value size, Heap * heap){
    heap_align(heap, 8);

    IntegerRuntimeObject * sizeInt = asInt(size);
    ArrayRuntimeObject * arrayRuntimeObject = (ArrayRuntimeObject *) allocate(heap, sizeof(ArrayRuntimeObject) + sizeInt->value * sizeof(Value));

    arrayRuntimeObject->type = TYPE_ARRAY;
    arrayRuntimeObject->length = sizeInt->value;

    return arrayRuntimeObject;
}

BcArrayRuntimeObject * allocate_bc_array(size_t size, Pointer initialValue, Heap * heap){
    heap_align(heap, 8);

    BcArrayRuntimeObject * arrayRuntimeObject = (BcArrayRuntimeObject *) allocate(heap, sizeof(BcArrayRuntimeObject) + size * sizeof(Value));

    arrayRuntimeObject->type = TYPE_ARRAY;
    arrayRuntimeObject->length = size;

    for(size_t i = 0; i < size; ++i){
        arrayRuntimeObject->elements[i] = initialValue;
    }

    return arrayRuntimeObject;
}

ObjectRuntimeObject * allocate_object(size_t memberCount, Heap * heap){
    heap_align(heap, 8);

    ObjectRuntimeObject * objectRuntimeObject = (ObjectRuntimeObject *) allocate(heap, sizeof(ObjectRuntimeObject) + memberCount * sizeof(Field));

    objectRuntimeObject->type = TYPE_OBJECT;
    objectRuntimeObject->fieldCount = memberCount;

    return objectRuntimeObject;
}

BcObjectRuntimeObject * allocate_bc_object(size_t memberCount, Heap * heap){
    heap_align(heap, 8);

    BcObjectRuntimeObject * objectRuntimeObject = (BcObjectRuntimeObject *) allocate(heap, sizeof(BcObjectRuntimeObject) + memberCount * sizeof(Field));

    objectRuntimeObject->type = TYPE_OBJECT;
    objectRuntimeObject->fieldCount = memberCount;

    return objectRuntimeObject;
}

AstFunction * load_function(Value functionValue){

    AstFunctionRuntimeValue * functionRuntimeValue = (AstFunctionRuntimeValue *) functionValue.valueInHeap;

    return functionRuntimeValue->function;
}

IntegerRuntimeObject * asInt(Value booleanValue){
    IntegerRuntimeObject * integerRuntimeObject = (IntegerRuntimeObject *) booleanValue.valueInHeap;
    return integerRuntimeObject;
}

BooleanRuntimeObject * asBool(Value booleanValue){
    BooleanRuntimeObject * booleanRuntimeObject = (BooleanRuntimeObject *) booleanValue.valueInHeap;
    return booleanRuntimeObject;
}

ArrayRuntimeObject * asArray(Value arrayValue){
    ArrayRuntimeObject * arrayRuntimeObject = (ArrayRuntimeObject *) arrayValue.valueInHeap;
    return arrayRuntimeObject;
}

ObjectRuntimeObject * asObject(Value objectValue){
    ObjectRuntimeObject * objectRuntimeObject = (ObjectRuntimeObject *) objectValue.valueInHeap;
    return objectRuntimeObject;
}

BcField * findBcField(BcObjectRuntimeObject * object, Str fieldName){

    for(size_t i = 0; i < object->fieldCount; ++i){
        BcField * targetField = object->fields + i;
        if(str_eq(targetField->name, fieldName)){
            return targetField;
        }
    }

    return NULL;
}
