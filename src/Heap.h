//
// Created by jerror on 3/8/23.
//

#include <stdio.h>
#include "parser.h"

#ifndef RUN_HEAP_H
#define RUN_HEAP_H

typedef unsigned char * Pointer;

typedef struct Value{
    // pointer na haldu
    unsigned char * valueInHeap;
} Value;

typedef struct Heap {
    unsigned char *mem;
    size_t pos;
    size_t capacity;
} Heap;

typedef enum{
    TYPE_BOOLEAN,
    TYPE_INTEGER,
    TYPE_NULL,
    TYPE_FUNCTION,
    TYPE_OBJECT,
    TYPE_ARRAY
} ValueType;

typedef struct RuntimeObject{
    ValueType type;
} RuntimeObject;

typedef struct BooleanRuntimeObject{
    ValueType type;
    bool value;
} BooleanRuntimeObject;

typedef struct IntegerRuntimeObject{
    ValueType type;
    i32 value;
} IntegerRuntimeObject;

typedef struct NullRuntimeValue{
    ValueType type;
} NullRuntimeValue;

typedef struct AstFunctionRuntimeValue{
    ValueType type;
    AstFunction * function;
} AstFunctionRuntimeValue;

typedef struct{
    ValueType type;
    size_t indexToConstPool;
} BcFunctionRuntimeValue;

typedef struct Field {
    Str name;
    Value value;
} Field;

typedef struct ObjectRuntimeObject{
    ValueType type;
    Value parent;
    size_t fieldCount;
    Field fields[];
} ObjectRuntimeObject;

typedef struct BcField {
    Str name;
    Pointer value;
} BcField;

typedef struct BcObjectRuntimeObject{
    ValueType type;
    Pointer parent;
    size_t fieldCount;
    BcField fields[];
} BcObjectRuntimeObject;

typedef struct ArrayRuntimeObject{
    ValueType type;
    size_t length;
    Value elements[];
} ArrayRuntimeObject;

typedef struct BcArrayRuntimeObject{
    ValueType type;
    size_t length;
    Pointer elements[];
} BcArrayRuntimeObject;

void heap_align(Heap * heap, size_t alignment);
RuntimeObject * convertPtrToRuntimeObject(Value valuePtr);

Heap * heap_alloc(size_t capacity);
void heap_destroy(Heap * heap);

unsigned char * allocate_integer(i32 value, Heap * heap);
unsigned char * allocate_boolean(bool value, Heap * heap);
unsigned char * allocate_null(Heap * heap);
unsigned char * allocate_ast_function(AstFunction * function, Heap * heap);
unsigned char * allocate_bc_function(size_t indexToConstPool, Heap * heap);
ArrayRuntimeObject * allocate_array(Value size, Heap * heap);
BcArrayRuntimeObject * allocate_bc_array(size_t size, Pointer initialValue, Heap * heap);
ObjectRuntimeObject * allocate_object(size_t memberCount, Heap * heap);
BcObjectRuntimeObject * allocate_bc_object(size_t memberCount, Heap * heap);


unsigned char * allocate(Heap * heap, size_t byteSize);

AstFunction * load_function(Value functionValue);

IntegerRuntimeObject * asInt(Value integerValue);
BooleanRuntimeObject * asBool(Value booleanValue);
ArrayRuntimeObject * asArray(Value arrayValue);
ObjectRuntimeObject * asObject(Value objectValue);

BcField * findBcField(BcObjectRuntimeObject * object, Str fieldName);

#endif //RUN_HEAP_H
