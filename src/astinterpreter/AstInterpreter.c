//
// Created by jerror on 3/7/23.
//

#include <stdio.h>
#include "AstInterpreter.h"
#include "src/Heap.h"
#include <assert.h>

void add_definition(Str name, Value definition, Stack *environmentStack) {
    map_put(name, definition, environmentStack->top->environment);
}

void update_definition(Str name, Value definition, Stack *environmentStack) {
    for (StackNode *currentNode = environmentStack->top; currentNode != NULL; currentNode = currentNode->next) {
        Value *value = map_find(name, currentNode->environment);
        if (value != NULL) {
            map_update(name, definition, currentNode->environment);
            return;
        }
    }

    printf("Fail to update definition");
    exit(1);
}

Value find_definition(Str name, Stack *environmentStack) {
    for (StackNode *currentNode = environmentStack->top; currentNode != NULL; currentNode = currentNode->next) {
        Value *value = map_find(name, currentNode->environment);
        if (value != NULL)
            return *value;
    }

    printf("FAIL: definition not found");
    exit(1);
}

// This method allocates list for returned list of value. After you are done with result free the list.
Value * evaluateNNodes(Ast **astNodes, size_t numberOfNodes, bool createEnvForEachNode, Heap *heap, Stack *environmentStack,
               Map *globalEnvironment) {

    Value *argumentValues = (Value *) malloc(numberOfNodes * sizeof(Value));

    // Evaluate all nodes one by one
    for (size_t i = 0; i < numberOfNodes; ++i) {

        if (createEnvForEachNode) {
            stack_push_new_environment(environmentStack);
        }

        Ast *currentNode = astNodes[i];

        Value currentValue = evaluate(currentNode, heap, environmentStack, globalEnvironment);

        argumentValues[i] = currentValue;

        if(createEnvForEachNode){
            stack_pop(environmentStack);
        }

    }

    return argumentValues;
}

Value createNull(Heap *heap) {
    unsigned char *ptr = allocate_null(heap);
    Value val = {ptr};
    return val;
}

bool isTruth(Value valueToCheck) {
    RuntimeObject *runtimeObject = convertPtrToRuntimeObject(valueToCheck);
    if (runtimeObject->type == TYPE_BOOLEAN) {
        return asBool(valueToCheck)->value == true;
    } else if (runtimeObject->type == TYPE_NULL) {
        return false;
    }
    return true;
}

void AstprintValue(Value valueToPrint) {
    RuntimeObject *runtimeObject = convertPtrToRuntimeObject(valueToPrint);

    switch (runtimeObject->type) {
        case TYPE_NULL:
            printf("%s", "null");
            break;
        case TYPE_BOOLEAN: {
            BooleanRuntimeObject *booleanRuntimeObject = asBool(valueToPrint);
            printf("%s", booleanRuntimeObject->value ? "true" : "false");
            break;
        }
        case TYPE_INTEGER: {
            IntegerRuntimeObject *integerRuntimeObject = asInt(valueToPrint);
            printf("%d", integerRuntimeObject->value);
            break;
        }
        case TYPE_ARRAY: {
            printf("[");
            ArrayRuntimeObject * array = asArray(valueToPrint);
            for (size_t i = 0; i < array->length; i++) {
                if (i != 0) {
                    printf(", ");
                }
                AstprintValue(array->elements[i]);
            }
            printf("]");
            break;
        }
        case TYPE_OBJECT: {
            printf("object(");
            ObjectRuntimeObject * objectRuntimeObject = (ObjectRuntimeObject *) runtimeObject;

            Value parent = objectRuntimeObject->parent;
            RuntimeObject * parentObject = convertPtrToRuntimeObject(parent);

            bool printComma = false;
            if (parentObject->type == TYPE_OBJECT) {
                printf("..=");
                AstprintValue(parent);
                printComma = true;
            }

            for (size_t i = 0; i < objectRuntimeObject->fieldCount; i++) {
                if (printComma) {
                    printf(", ");
                }

                Field * field = objectRuntimeObject->fields + i;
                Str name = field->name;
                printf("%.*s=", (int)name.len, name.str);
                AstprintValue(field->value);
                printComma = true;
            }
            printf(")");
            break;
        }
        case TYPE_FUNCTION:
            printf("function");
            break;
    }
}

void AstexecutePrint(Str format, Value *arguments, size_t argumentCount) {

    bool in_escape = false;
    size_t arg_index = 0;

    for (size_t i = 0; i < format.len; ++i) {
        u8 c = format.str[i];
        u8 outputChar = c;
        if (in_escape) {
            in_escape = false;
            switch (c) {
                case 'n':
                    outputChar = '\n';
                    break;
                case 't':
                    outputChar = '\t';
                    break;
                case 'r':
                    outputChar = '\r';
                    break;
                case '~':
                    outputChar = '~';
                    break;
                case '"':
                    outputChar = '"';
                    break;
                case '\\':
                    outputChar = '\\';
                    break;
                default: {
                    printf("\n");
                    printf("ESCAPE SEQUENCE NOT RECOGNIZED\n");
                    exit(1);
                }
            }
            putchar(outputChar);
        } else {
            switch (c) {
                case '\\':
                    in_escape = true;
                    break;
                case '~':
                    if (arg_index >= argumentCount) {
                        printf("\n");
                        printf("There are too little arguments\n");
                        exit(1);
                    }
                    AstprintValue(arguments[arg_index]);
                    arg_index += 1;
                    break;
                default:
                    putchar(outputChar);
            }
        }
    }
    fflush(stdout);
}

Value
AstcallFunction(Value functionDefinition, Value *arguments, size_t argumentCount, Value objectTarget, Heap *heap, Map *globalEnvironment) {

    // Load function from heap
    AstFunction *loadedFunction = load_function(functionDefinition);


    assert(loadedFunction->parameter_cnt == argumentCount);


    // Create new environment with only global environment as its parent and add arguments to it with names from parameters
    Stack *functionScope = stack_alloc();
    stack_push_environment(globalEnvironment, functionScope);

    // Create function scope

    stack_push_new_environment(functionScope);
    for (size_t i = 0; i < loadedFunction->parameter_cnt; ++i) {
        Str parameterName = loadedFunction->parameters[i];
        Value parameterValue = arguments[i];

        // Add parameter to function scope
        add_definition(parameterName, parameterValue, functionScope);
    }

    // Add this reference
    add_definition(STR("this"), objectTarget, functionScope);

    // Execute function
    Value returnValue = evaluate(loadedFunction->body, heap, functionScope, globalEnvironment);

    // Remove function environment but keep global scope
    stack_destroy_but_keep_last_env(functionScope);

    return returnValue;
}

Field * findField(ObjectRuntimeObject * object, Str fieldName){

    for(size_t i = 0; i < object->fieldCount; ++i){
        Field * targetField = object->fields + i;
        if(str_cmp(targetField->name, fieldName)){
            return targetField;
        }
    }

    return NULL;
}

Value
callMethod(Value object, Str methodName, bool isField, Value *arguments, size_t argumentCount, Heap *heap,
           Map *globalEnvironment) {

    // Load function from heap
    RuntimeObject *runtimeObject = convertPtrToRuntimeObject(object);

    if(runtimeObject->type == TYPE_OBJECT){
        ObjectRuntimeObject * target = asObject(object);
        Field * targetField = findField(target, methodName);

        if(targetField != NULL){
            Value fieldValue = targetField->value;
            if(isField){
                // Access field
                return fieldValue;
            }else{
                assert(((RuntimeObject *)fieldValue.valueInHeap)->type == TYPE_FUNCTION);
                // Call method
                return AstcallFunction(fieldValue, arguments, argumentCount, object, heap, globalEnvironment);
            }
        }

        printf("Field not found\n");
        exit(1);
    }


    // Call array methods
    if (runtimeObject->type == TYPE_ARRAY) {
        assert(argumentCount >= 1);

        ArrayRuntimeObject *array = asArray(object);
        IntegerRuntimeObject *index = asInt(arguments[0]);

        if (str_cmp(methodName, STR("get"))) {
            Value val = array->elements[index->value];
            return val;
        } else if (str_cmp(methodName, STR("set"))) {
            assert(argumentCount == 2);

            array->elements[index->value] = arguments[1];
            return createNull(heap);
        }
    }

    // Call builtin functions
    if (argumentCount != 1) {
        printf("Invalid number of arguments\n");
        exit(1);
    }

    Value rightSide = arguments[0];

    // Operators defined on multiple types
    if(str_cmp(methodName, STR("=="))){
        if(runtimeObject->type != convertPtrToRuntimeObject(rightSide)->type){
            Value val = {allocate_boolean(false, heap)};
            return val;
        }

        // Types are equal
        if(runtimeObject->type == TYPE_NULL){
            Value val = {allocate_boolean(true, heap)};
            return val;
        }
        else if(runtimeObject->type == TYPE_INTEGER){
            Value val = {allocate_boolean(asInt(object)->value == asInt(rightSide)->value, heap)};
            return val;
        }else if(runtimeObject->type == TYPE_BOOLEAN){
            Value val = {allocate_boolean(asBool(object)->value == asBool(rightSide)->value, heap)};
            return val;
        }

    }else if(str_cmp(methodName, STR("!="))){
        if(runtimeObject->type != convertPtrToRuntimeObject(rightSide)->type){
            Value val = {allocate_boolean(true, heap)};
            return val;
        }

        // Types are equal
        if(runtimeObject->type == TYPE_NULL){
            Value val = {allocate_boolean(false, heap)};
            return val;
        }else if(runtimeObject->type == TYPE_INTEGER){
            Value val = {allocate_boolean(asInt(object)->value != asInt(rightSide)->value, heap)};
            return val;
        }else if(runtimeObject->type == TYPE_BOOLEAN){
            Value val = {allocate_boolean(asBool(object)->value != asBool(rightSide)->value, heap)};
            return val;
        }
    }

    if (runtimeObject->type == TYPE_INTEGER) {
        IntegerRuntimeObject *leftInt = asInt(object);
        IntegerRuntimeObject *rightInt = asInt(rightSide);
        if (str_cmp(methodName, STR("+"))) {
            Value val = {allocate_integer(leftInt->value + rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("-"))) {
            Value val = {allocate_integer(leftInt->value - rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("*"))) {
            Value val = {allocate_integer(leftInt->value * rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("/"))) {
            Value val = {allocate_integer(leftInt->value / rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("%"))) {
            Value val = {allocate_integer(leftInt->value % rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("<="))) {
            Value val = {allocate_boolean(leftInt->value <= rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR(">="))) {
            Value val = {allocate_boolean(leftInt->value >= rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("<"))) {
            Value val = {allocate_boolean(leftInt->value < rightInt->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR(">"))) {
            Value val = {allocate_boolean(leftInt->value > rightInt->value, heap)};
            return val;
        }
    } else if (runtimeObject->type == TYPE_BOOLEAN) {
        BooleanRuntimeObject *leftBool = asBool(object);
        BooleanRuntimeObject *rightBool = asBool(rightSide);
        if (str_cmp(methodName, STR("&"))) {
            Value val = {allocate_boolean(leftBool->value & rightBool->value, heap)};
            return val;
        } else if (str_cmp(methodName, STR("|"))) {
            Value val = {allocate_boolean(leftBool->value | rightBool->value, heap)};
            return val;
        }
    }

    printf("Method not recognized\n");
    exit(1);


}

Value evaluate(Ast *ast, Heap *heap, Stack *environmentStack, Map *globalEnvironment) {

    switch (ast->kind) {
        case AST_NULL: {
            return createNull(heap);
        };
            break;
        case AST_BOOLEAN: {
            AstBoolean *boolean = (AstBoolean *) ast;
            unsigned char *ptr = allocate_boolean(boolean->value, heap);
            Value val = {ptr};
            return val;
        };
            break;
        case AST_INTEGER: {
            AstInteger *integer = (AstInteger *) ast;
            unsigned char *integerPtr = allocate_integer(integer->value, heap);
            Value integerVal = {integerPtr};
            return integerVal;
        };
            break;
        case AST_ARRAY: {
            AstArray *array = (AstArray *) ast;

            Value size = evaluate(array->size, heap, environmentStack, globalEnvironment);

            ArrayRuntimeObject *arrayRuntimeObject = allocate_array(size, heap);

            IntegerRuntimeObject *intSize = asInt(size);
            for (int i = 0; i < intSize->value; ++i) {
                stack_push_new_environment(environmentStack);

                arrayRuntimeObject->elements[i] = evaluate(array->initializer, heap, environmentStack,
                                                           globalEnvironment);

                stack_pop(environmentStack);
            }

            Value arrayValue = {(unsigned char *) arrayRuntimeObject};
            return arrayValue;
        }
            break;
        case AST_OBJECT: {
            AstObject *object = (AstObject *) ast;

            Value parent = evaluate(object->extends, heap, environmentStack, globalEnvironment);

            ObjectRuntimeObject *objectRuntimeObject = allocate_object(object->member_cnt, heap);

            objectRuntimeObject->parent = parent;

            Value *members = evaluateNNodes(object->members, object->member_cnt, true, heap, environmentStack,
                                            globalEnvironment);
            for (size_t i = 0; i < object->member_cnt; ++i) {

                AstDefinition * definitionNode = (AstDefinition *) object->members[i];
                Str memberName = definitionNode->name;

                objectRuntimeObject->fields[i].name = memberName;
                objectRuntimeObject->fields[i].value = members[i];
            }

            Value objectValue = {(unsigned char *) objectRuntimeObject};
            return objectValue;
        }
            break;
        case AST_FUNCTION: {
            AstFunction *function = (AstFunction *) ast;

            unsigned char *functionPtr = allocate_ast_function(function, heap);
            Value functionVal = {functionPtr};

            return functionVal;
        };
            break;
        case AST_DEFINITION: {
            AstDefinition *definition = (AstDefinition *) ast;

            Str name = definition->name;
            Value evaluatedValue = evaluate(definition->value, heap, environmentStack, globalEnvironment);

            add_definition(name, evaluatedValue, environmentStack);

            return evaluatedValue;
        };
            break;
        case AST_VARIABLE_ACCESS: {
            AstVariableAccess *variableAccess = (AstVariableAccess *) ast;

            Str name = variableAccess->name;

            Value variable = find_definition(name, environmentStack);
            return variable;
        };
            break;
        case AST_VARIABLE_ASSIGNMENT: {
            AstVariableAssignment *variableAssignment = (AstVariableAssignment *) ast;

            Str name = variableAssignment->name;
            Value evaluatedValue = evaluate(variableAssignment->value, heap, environmentStack, globalEnvironment);

            update_definition(name, evaluatedValue, environmentStack);

            return evaluatedValue;
        };
            break;
        case AST_INDEX_ACCESS: {
            AstIndexAccess *indexAccess = (AstIndexAccess *) ast;

            Value object = evaluate(indexAccess->object, heap, environmentStack, globalEnvironment);
            Value index = evaluate(indexAccess->index, heap, environmentStack, globalEnvironment);

            return callMethod(object, STR("get"), false, &index, 1, heap, globalEnvironment);
        }
            break;
        case AST_INDEX_ASSIGNMENT: {
            AstIndexAssignment *indexAssignment = (AstIndexAssignment *) ast;

            Value object = evaluate(indexAssignment->object, heap, environmentStack, globalEnvironment);
            Value index = evaluate(indexAssignment->index, heap, environmentStack, globalEnvironment);
            Value newValue = evaluate(indexAssignment->value, heap, environmentStack, globalEnvironment);

            Value setArguments[2] = {index, newValue};
            return callMethod(object, STR("set"), false, setArguments, 2, heap, globalEnvironment);
        }
            break;
        case AST_FIELD_ACCESS: {
            AstFieldAccess * fieldAccess = (AstFieldAccess *) ast;

            Value object = evaluate(fieldAccess->object, heap, environmentStack, globalEnvironment);

            return callMethod(object, fieldAccess->field, true, NULL, 0, heap, globalEnvironment);
        }
            break;
        case AST_FIELD_ASSIGNMENT: {
            AstFieldAssignment *fieldAssignment = (AstFieldAssignment *) ast;

            Value object = evaluate(fieldAssignment->object, heap, environmentStack, globalEnvironment);

            ObjectRuntimeObject * runtimeObject = asObject(object);
            Field * foundField = findField(runtimeObject, fieldAssignment->field);
            if(foundField == NULL){
                printf("FIELD not found\n");
                exit(1);
            }

            Value newValue = evaluate(fieldAssignment->value, heap, environmentStack, globalEnvironment);
            foundField->value = newValue;

            return createNull(heap);

        }
            break;
        case AST_FUNCTION_CALL: {
            AstFunctionCall *functionCall = (AstFunctionCall *) ast;

            // Find function definition in current environment
            Value function = evaluate(functionCall->function, heap, environmentStack, globalEnvironment);

            // Evaluate arguments
            Value *argumentList = evaluateNNodes(functionCall->arguments, functionCall->argument_cnt, false, heap,
                                                 environmentStack, globalEnvironment);

            Value functionReturnValue = AstcallFunction(function, argumentList, functionCall->argument_cnt,
                                                        createNull(heap), heap,
                                                        globalEnvironment);

            // Deallocate argument list
            free(argumentList);

            return functionReturnValue;
        };
            break;
        case AST_METHOD_CALL: {
            AstMethodCall *methodCall = (AstMethodCall *) ast;

            Value object = evaluate(methodCall->object, heap, environmentStack, globalEnvironment);

            Value *arguments = evaluateNNodes(methodCall->arguments, methodCall->argument_cnt, false, heap, environmentStack,
                                              globalEnvironment);

            return callMethod(object, methodCall->name, false, arguments, methodCall->argument_cnt, heap, globalEnvironment);
        }
            break;
        case AST_CONDITIONAL: {
            AstConditional *conditional = (AstConditional *) ast;

            Value condition = evaluate(conditional->condition, heap, environmentStack, globalEnvironment);

            Value result;

            stack_push_new_environment(environmentStack);

            if (isTruth(condition)) {
                result = evaluate(conditional->consequent, heap, environmentStack, globalEnvironment);
            } else {
                result = evaluate(conditional->alternative, heap, environmentStack, globalEnvironment);
            }

            stack_pop(environmentStack);
            return result;
        };
            break;
        case AST_LOOP: {
            AstLoop *loop = (AstLoop *) ast;

            while (isTruth(evaluate(loop->condition, heap, environmentStack, globalEnvironment))) {
                stack_push_new_environment(environmentStack);

                evaluate(loop->body, heap, environmentStack, globalEnvironment);

                stack_pop(environmentStack);
            }

            Value null = createNull(heap);
            return null;
        };
            break;
        case AST_PRINT: {
            AstPrint *print = (AstPrint *) ast;

            // Interpret all arguments
            Value *arguments = evaluateNNodes(print->arguments, print->argument_cnt, false, heap, environmentStack,
                                              globalEnvironment);

            AstexecutePrint(print->format, arguments, print->argument_cnt);

            free(arguments);

            return createNull(heap);
        };
            break;
        case AST_BLOCK: {
            AstBlock *block = (AstBlock *) ast;

            stack_push_new_environment(environmentStack);

            Value *values = evaluateNNodes(block->expressions, block->expression_cnt, false, heap, environmentStack,
                                           globalEnvironment);
            Value lastValue = values[block->expression_cnt - 1];

            stack_pop(environmentStack);
            free(values);

            return lastValue;
        };
            break;
        case AST_TOP: {
            AstTop *top = (AstTop *) ast;

            Value *values = evaluateNNodes(top->expressions, top->expression_cnt, false, heap, environmentStack,
                                           globalEnvironment);
            Value lastValue = values[top->expression_cnt - 1];

            free(values);
            return lastValue;

        };
            break;
    }
    printf("FAIL: Eval not processed\n");
    exit(1);
}

void astInterpret(Ast *ast) {
//    printf("RUNNING AST interpreter\n");

    // Allocate structures
    Heap * heap = heap_alloc(1000000000);
    Stack *environmentStack = stack_alloc();

    // Create global environment
    stack_push_new_environment(environmentStack);
    Map *globalEnv = environmentStack->top->environment;

    // Run evaluation
    evaluate(ast, heap, environmentStack, globalEnv);

    // Cleanup
    // Destroys the global map with it
    stack_destroy(environmentStack);
    heap_destroy(heap);

}
