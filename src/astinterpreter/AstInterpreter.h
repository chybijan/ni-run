//
// Created by jerror on 3/7/23.
//

#ifndef RUN_ASTINTERPRETER_H
#define RUN_ASTINTERPRETER_H

#include "parser.h"
#include "src/Heap.h"
#include "Stack.h"

void add_definition(Str name, Value definition, Stack * environmentStack);
void update_definition(Str name, Value definition, Stack * environmentStack);
Value find_definition(Str name, Stack * environmentStack);

Value evaluate(Ast * ast, Heap * heap, Stack * environmentStack, Map * globalEnvironment);
void astInterpret(Ast * ast);

#endif //RUN_ASTINTERPRETER_H
