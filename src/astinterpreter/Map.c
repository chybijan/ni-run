//
// Created by jerror on 3/8/23.
//

#include "Map.h"

Map * map_alloc(){

    Map * map = (Map *) malloc(sizeof(Map));

    map->size = 0;
    map->capacity = 0;
    map->keys = NULL;
    map->values = NULL;

    return map;
}

void map_clear(Map * map){
    free(map->keys);
    free(map->values);
}

void map_destroy(Map * map){
    map_clear(map);
    free(map);
}

void map_update(Str key, Value value, Map * map){
    Value * mapEntry = map_find(key, map);
    *mapEntry = value;
}

void map_put(Str key, Value value, Map * map){
   if(map->size == map->capacity){

       // Alloc new places
       size_t newCapacity = (map->capacity + 1) * 2;

       Map * newMap = map_alloc();

       newMap->keys = (Str *) malloc(newCapacity * sizeof(Str));
       newMap->values = (Value *) malloc(newCapacity * sizeof(Value));
       newMap->capacity = newCapacity;

       // Copy old values into new ones
       for(size_t i = 0; i < map->capacity; ++i){
           newMap->keys[i] = map->keys[i];
           newMap->values[i] = map->values[i];
       }

       // Copy values to old map and destroy new tmp map
       Str * tmpIndexes = map->keys;
       Value * tmpValues = map->values;

       map->capacity = newCapacity;
       map->keys = newMap->keys;
       map->values = newMap->values;

       newMap->values = tmpValues;
       newMap->keys = tmpIndexes;

       map_destroy(newMap);
   }

   // Add new element
   map->keys[map->size] = key;
   map->values[map->size] = value;
   map->size++;
}

Value * map_find(Str key, Map * map){
    for(size_t i = 0; i < map->size; ++i){
        Str mapKey = map->keys[i];
        if(str_cmp(mapKey, key)){
            return &map->values[i];
        }
    }

    return NULL;
}
