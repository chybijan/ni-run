//
// Created by jerror on 3/8/23.
//

#include "parser.h"
#include "src/Heap.h"

#ifndef RUN_MAP_H
#define RUN_MAP_H

typedef struct Map{

    Str * keys;
    Value * values;

    size_t size;
    size_t capacity;

} Map;


Map * map_alloc();
void map_destroy(Map * map);

void map_update(Str key, Value value, Map * map);
void map_put(Str key, Value value, Map * map);
Value * map_find(Str key, Map * map);

#endif //RUN_MAP_H
