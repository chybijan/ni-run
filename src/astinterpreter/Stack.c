//
// Created by jerror on 3/8/23.
//

#include "Stack.h"
#include "stdlib.h"
#include <stdio.h>


Stack * stack_alloc(){

    Stack * newStack = (Stack *) malloc(sizeof(Stack));
    newStack->top = NULL;

    return newStack;
}

void stack_node_destroy(StackNode * stackNode){
    map_destroy(stackNode->environment);
    free(stackNode);
}

void stack_node_destroy_but_keep_map(StackNode * stackNode){
    free(stackNode);
}

void stack_destroy(Stack * stack){

    // Destroy stack nodes
    StackNode * currentNode = stack->top;
    while(currentNode != NULL){
        StackNode * nextNode = currentNode->next;
        stack_node_destroy(currentNode);
        currentNode = nextNode;
    }

    // Destroy stack
    free(stack);
}

void stack_destroy_but_keep_last_env(Stack * stack){

    // Destroy stack nodes
    StackNode * currentNode = stack->top;
    while(currentNode != NULL){
        StackNode * nextNode = currentNode->next;
        if(nextNode == NULL){
            // Keep last env
            stack_node_destroy_but_keep_map(currentNode);
        }else{
            stack_node_destroy(currentNode);
        }
        currentNode = nextNode;
    }

    // Destroy stack
    free(stack);
}

void stack_push_new_environment(Stack * stack){
    Map * newEnvironment = map_alloc();
    stack_push_environment(newEnvironment, stack);
}

void stack_push_environment(Map * environment, Stack * stack){
    StackNode * stackNode = (StackNode *) malloc(sizeof(StackNode));

    // Setup stack node
    stackNode->environment = environment;
    stackNode->next = stack->top;

    // Set new top of the stack
    stack->top = stackNode;
}


void stack_pop(Stack * stack){

    // Get top
    StackNode * currentTop = stack->top;

    if(currentTop == NULL){
        printf("FAIL: CANNOT POP EMPTY STACK");
        exit(1);
    }

    // Set new top
    stack->top = currentTop->next;

    // Destroy old stackNode
    stack_node_destroy(currentTop);

}