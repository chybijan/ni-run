//
// Created by jerror on 3/8/23.
//

#ifndef RUN_STACK_H
#define RUN_STACK_H

#include "Map.h"

typedef struct StackNode{

    struct StackNode * next;
    Map * environment;

} StackNode;

typedef struct Stack{
    StackNode * top;
} Stack;

Stack * stack_alloc();
void stack_destroy(Stack * stack);
void stack_destroy_but_keep_last_env(Stack * stack);
void stack_node_destroy(StackNode * stackNode);
void stack_node_destroy_but_keep_map(StackNode * stackNode);

void stack_push_new_environment(Stack * stack);
void stack_push_environment(Map * environment, Stack * stack);
void stack_pop(Stack * stack);


#endif //RUN_STACK_H
