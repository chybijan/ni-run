//
// Created by jerror on 4/4/23.
//

#include <stdio.h>
#include <assert.h>
#include "BcCompiler.h"
#include "Program.h"
#include "ByteVector.h"

// Drop se volá za každým expressionem v begin a end blocku, kromě poslední. To samé v topu
// Má se dělat nějaké počítání identifikátorů. Budu počítat proměnně (to je offset ve frame stacku), na konci begin end blocku se smaže číslování proměnných uvnitř
// Na konci begin a end, nebo return, tak musím zařídit že na zásobníku bude jediná hodnota, kterou vrací, což se asi stane defaultně
// Deduplikace konstant není třeba, protože moje i referenční implementace na to nespoléhá. Ale při benchmarcích se to musí deduplikovat
//

void compileNode(Ast * node, Program * program, FunctionConst * currentFunction);

Byte * addByte(FunctionConst * currentFunction, Byte byte){
    return push_back_byte(currentFunction->body, byte);
}

Byte * addU16(FunctionConst * currentFunction, u16 toAdd){
    return push_back_u16(currentFunction->body, toAdd);
}


void addConstantOp(Program * program, FunctionConst * target, Constant constant){

    // Add constant to constant pool or get already created constant
    size_t constantIndex = addConstant(program, constant);

    // Push constant onto the stack
    addByte(target, CONSTANT_OP);
    addU16(target, constantIndex);
}

//char* concat(const char *s1, const char *s2)
//{
//    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
//    strcpy(result, s1);
//    strcat(result, s2);
//    return result;
//}
//
//char * findUniqueName(LocalMap * map){
//    char * namePiece = "aoisdngoiybunqbqwbr";
//
//    char * wholeName = concat(namePiece, "");
//
//    for(int found = findLocal(map, STR(wholeName)); found != -1; found = findLocal(map, STR(wholeName))){
//
//        char * newName = concat(wholeName, namePiece);
//        free(wholeName);
//        wholeName = newName;
//    }
//
//    return wholeName;
//
//}

size_t defineVar(FunctionConst * functionConst, Str localName){

    size_t i = addLocal(functionConst->locals, localName);

    if(i + 1 > functionConst->localCount){
        functionConst->localCount = i + 1;
    }

    return i;

}

int findVar(FunctionConst * functionConst, Str localName){
    return findLocal(functionConst->locals, localName);
}

void compileNode(Ast * node, Program * program, FunctionConst * currentFunction){
    if(currentFunction == NULL && node->kind != AST_TOP){

        FunctionConst mainFunc;
        mainFunc.localCount = 0;
        mainFunc.paramCount = 1;
        mainFunc.locals = localMap_alloc();
        mainFunc.body = createVector();
        mainFunc.isMain = true;
        mainFunc.objectConstructionInProgress = false;

        defineVar(&mainFunc, STR("this"));

        // Create null constant
        Constant nullConstant;
        nullConstant.tag = NULL_CONST;

        addConstantOp(program, &mainFunc, nullConstant);

        // Add return to main
        addByte(&mainFunc, RETURN_OP);

        // Add main to const pool
        Constant mainFuncConst;
        mainFuncConst.tag = FUNCTION_CONST;
        mainFuncConst.function = mainFunc;

        size_t entryPoint = addConstant(program, mainFuncConst);

        // Set entry point
        program->entryPoint = entryPoint;
        return;
    }

    switch (node->kind){
        case AST_NULL:{

            // Create null constant
            Constant nullConstant;
            nullConstant.tag = NULL_CONST;

            addConstantOp(program, currentFunction, nullConstant);

            break;
        };
        case AST_BOOLEAN:{

            AstBoolean * boolean = (AstBoolean *) node;

            // Create boolean constant
            Constant boolConstant;
            boolConstant.tag = BOOLEAN_CONST;
            boolConstant.boolean = boolean->value;

            addConstantOp(program, currentFunction, boolConstant);
            break;

        };
        case AST_INTEGER:{
            AstInteger * integer = (AstInteger *) node;

            // Create int constant
            Constant intConst;
            intConst.tag = INTEGER_CONST;
            intConst.integer = integer->value;

            addConstantOp(program, currentFunction, intConst);
            break;
        };
        case AST_ARRAY:{
            AstArray * array = (AstArray *) node;

            compileNode(array->size, program, currentFunction);
            // Create null constant
            Constant nullConstant;
            nullConstant.tag = NULL_CONST;
            addConstantOp(program, currentFunction, nullConstant);

//            compileNode(array->initializer, program, currentFunction);
            addByte(currentFunction, ARRAY_OP);

            Str arrayName = STR("temp1");//findUniqueName(currentFunction->locals);

            bool isMain = currentFunction->isMain;
            bool objConsInProgress = currentFunction->objectConstructionInProgress;
            ClassConst * classConst = currentFunction->classConstructed;
            currentFunction->isMain = false;
            currentFunction->objectConstructionInProgress = false;

            // Save array to local variable
            size_t localIndex = defineVar(currentFunction, arrayName);
            addByte(currentFunction, SET_LOCAL_OP);
            addU16(currentFunction, localIndex);

            addByte(currentFunction, DROP_OP);


            // array->size - 1
            AstMethodCall sub1 = (AstMethodCall) {
                    .base = (Ast) { .kind = AST_METHOD_CALL },
                    .object = array->size,
                    .name = STR("-"),
                    .arguments = (Ast*[]) {
                            &(AstInteger) {
                                    .base = (Ast) { .kind = AST_INTEGER },
                                    .value = 0,
                            }.base,
                    },
                    .argument_cnt = 1,
            };

            // temp2 = array.size - 1;
            Str sizeName = STR("temp2");//findUniqueName(currentFunction->locals);
            AstDefinition sizeAst = (AstDefinition) {
                    .base = (Ast) { .kind = AST_DEFINITION },
                    .name = sizeName,
                    .value = &sub1.base,
            };
            compileNode(&sizeAst.base, program, currentFunction);

            addByte(currentFunction, DROP_OP);

            // while(temp2 > 0){
            //      array[array->size - temp2] = init();
            //      temp2 = temp2 - 1
            // }

            // uniqueName > 0
            AstMethodCall condition = (AstMethodCall) {
                    .base = (Ast) { .kind = AST_METHOD_CALL },
                    .object = &(AstVariableAccess) {
                            .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                            .name = sizeName,
                    }.base,
                    .name = STR(">"),
                    .arguments = (Ast*[]) {
                        &(AstInteger) {
                                .base = (Ast) { .kind = AST_INTEGER },
                                .value = 0,
                        }.base,
                    },
                    .argument_cnt = 1,
            };

            // array->size - uniqueName
            AstMethodCall sub = (AstMethodCall) {
                    .base = (Ast) { .kind = AST_METHOD_CALL },
                    .object = array->size,
                    .name = STR("-"),
                    .arguments = (Ast*[]) {
                        &(AstVariableAccess) {
                                .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                                .name = sizeName,
                        }.base,
                    },
                    .argument_cnt = 1,
            };

            AstBlock loopBody = (AstBlock) {
                    .base = (Ast) { .kind = AST_BLOCK },
                    .expressions = (Ast*[]) {
                            &(AstIndexAssignment) {
                                    .base = (Ast) { .kind = AST_INDEX_ASSIGNMENT },
                                    .object = &(AstVariableAccess) {
                                            .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                                            .name = arrayName,
                                    }.base,
                                    .index = &sub.base,
                                    .value = array->initializer
                            }.base,
                            &(AstVariableAssignment) {
                                .base = (Ast) { .kind = AST_VARIABLE_ASSIGNMENT },
                                .name = sizeName,
                                .value = &(AstMethodCall) {
                                        .base = (Ast) { .kind = AST_METHOD_CALL },
                                        .object = &(AstVariableAccess) {
                                                .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                                                .name = sizeName,
                                        }.base,
                                        .name = STR("-"),
                                        .arguments = (Ast*[]) {
                                            &(AstInteger) {
                                                    .base = (Ast) { .kind = AST_INTEGER },
                                                    .value = 1,
                                            }.base,
                                        },
                                        .argument_cnt = 1,
                                }.base,
                            }.base,
                    },
                    .expression_cnt = 2,
            };

            AstLoop loop = (AstLoop) {
                    .base = (Ast) { .kind = AST_LOOP },
                    .condition = &condition.base,
                    .body = &loopBody.base,
            };

            compileNode(&loop.base, program, currentFunction);
            addByte(currentFunction, DROP_OP);


            // Get value from array
            compileNode(&(AstVariableAccess) {
                    .base = (Ast) { .kind = AST_VARIABLE_ACCESS },
                    .name = arrayName,
            }.base, program, currentFunction);


            currentFunction->isMain = isMain;
            currentFunction->objectConstructionInProgress = objConsInProgress;
            currentFunction->classConstructed = classConst;

//            free(arrayName);
//            free(sizeName);

            break;
        };
        case AST_OBJECT:{
            AstObject * object = (AstObject *) node;

            // Create constant
            Constant objConst;
            objConst.tag = CLASS_CONST;
            objConst.class.fieldMap = fieldMap_alloc();

            // Open new block in order to delete locals defined during object creation
            size_t localCounter = currentFunction->locals->size;
            bool isMain = currentFunction->isMain;
            currentFunction->isMain = false;
            ClassConst * beforeClass = currentFunction->classConstructed;
            bool objConstrInProgressBefore = currentFunction->objectConstructionInProgress;

            currentFunction->objectConstructionInProgress = true;
            currentFunction->classConstructed = &objConst.class;

            // Compile parent
            compileNode(object->extends, program, currentFunction);

            // Compile members
            for(size_t i = 0; i < object->member_cnt; ++i){
                compileNode(object->members[i], program, currentFunction);
            }

            // Close block in order to restore previous state of the compiler
            currentFunction->isMain = isMain;
            restoreLocals(currentFunction->locals, localCounter);
            currentFunction->objectConstructionInProgress = objConstrInProgressBefore;
            currentFunction->classConstructed = beforeClass;

            assert(objConst.class.fieldMap->size == object->member_cnt);

            // Add constant to constant pool or get already created constant
            size_t constantIndex = addConstant(program, objConst);

            // Push constant onto the stack
            addByte(currentFunction, OBJECT_OP);
            addU16(currentFunction, constantIndex);

            break;
        };
        case AST_FUNCTION:{
            AstFunction * func = (AstFunction *) node;

            // Create function constant
            Constant function;
            function.tag = FUNCTION_CONST;
            function.function.localCount = 0;
            function.function.paramCount = func->parameter_cnt + 1; // Add 'this' to function count
            function.function.locals = localMap_alloc();
            function.function.body = createVector();
            function.function.isMain = false;
            function.function.objectConstructionInProgress = false;

            // Define local var 'this'
            defineVar(&(function.function), STR("this"));

            // Define rest of local variables
            for(size_t i = 0; i < func->parameter_cnt; ++i){
                defineVar(&(function.function), func->parameters[i]);
            }

            // Compile body of function
            compileNode(func->body, program, &(function.function));

            // Add return
            addByte(&(function.function), RETURN_OP);

            addConstantOp(program, currentFunction, function);
            break;
        }
        case AST_DEFINITION:{

            AstDefinition * definition = (AstDefinition *) node;

            if(currentFunction->objectConstructionInProgress){

                // Enter block
                bool objConstrInProgess = currentFunction->objectConstructionInProgress;
                currentFunction->objectConstructionInProgress = false;
                size_t localCounter = currentFunction->locals->size;
                bool isMain = currentFunction->isMain;
                currentFunction->isMain = false;

                compileNode(definition->value, program, currentFunction);

                ClassConst * classConst = currentFunction->classConstructed;

                Constant fieldName;
                fieldName.tag = STRING_CONST;
                fieldName.string = definition->name;
                size_t constantIndex = addConstant(program, fieldName);

                addField(classConst->fieldMap, fieldName.string, constantIndex);

                // Exit block
                currentFunction->isMain = isMain;
                restoreLocals(currentFunction->locals, localCounter);
                currentFunction->objectConstructionInProgress = objConstrInProgess;

            }else if(currentFunction->isMain){

               compileNode(definition->value, program, currentFunction);

                // Define function
                size_t constantPoolIndex = addGlobal(program, definition->name);


               addByte(currentFunction, SET_GLOBAL_OP);
               addU16(currentFunction, constantPoolIndex);

           }else{
               // Set value of local to value
               // Compile value
               compileNode(definition->value, program, currentFunction);

                // Add local to current function
                size_t localIndex = defineVar(currentFunction, definition->name);

                addByte(currentFunction, SET_LOCAL_OP);
               addU16(currentFunction, localIndex);

           }

            break;
        };
        case AST_VARIABLE_ACCESS:{

            AstVariableAccess * access = (AstVariableAccess *) node;

            int localIndex = findVar(currentFunction, access->name);

            if(localIndex == -1){
                // Variable is global

                // Add global
                size_t constantPoolIndex = addGlobal(program, access->name);

                addByte(currentFunction, GET_GLOBAL_OP);
                addU16(currentFunction, constantPoolIndex);
            }else{

                // Variable is local
                addByte(currentFunction, GET_LOCAL_OP);
                addU16(currentFunction, localIndex);
            }

            break;

        };
        case AST_VARIABLE_ASSIGNMENT:{

            AstVariableAssignment * assignment = (AstVariableAssignment *) node;

            // Compile value
            compileNode(assignment->value, program, currentFunction);

            int localIndex = findVar(currentFunction, assignment->name);

            if(localIndex == -1){
                // Variable is global

                // Add global
                size_t constantPoolIndex = addGlobal(program, assignment->name);

                addByte(currentFunction, SET_GLOBAL_OP);
                addU16(currentFunction, constantPoolIndex);
            }else{
                // Variable is local

                addByte(currentFunction, SET_LOCAL_OP);
                addU16(currentFunction, localIndex);
            }

            break;
        };
        case AST_INDEX_ACCESS:{

            AstIndexAccess * acc = (AstIndexAccess *) node;

            compileNode(acc->object, program, currentFunction);

            compileNode(acc->index, program, currentFunction);

            // Find constant index of field name
            Constant fieldName;
            fieldName.tag = STRING_CONST;
            fieldName.string = STR("get");
            u16 constIndex = addConstant(program, fieldName);

            addByte(currentFunction, CALL_METHOD_OP);
            addU16(currentFunction, constIndex);
            addByte(currentFunction, 2);

            break;
        };
        case AST_INDEX_ASSIGNMENT:{

            AstIndexAssignment * ass = (AstIndexAssignment *) node;

            compileNode(ass->object, program, currentFunction);

            compileNode(ass->index, program, currentFunction);

            compileNode(ass->value, program, currentFunction);

            // Find constant index of field name
            Constant fieldName;
            fieldName.tag = STRING_CONST;
            fieldName.string = STR("set");
            u16 constIndex = addConstant(program, fieldName);

            addByte(currentFunction, CALL_METHOD_OP);
            addU16(currentFunction, constIndex);
            addByte(currentFunction, 3);

            break;
        };
        case AST_FIELD_ACCESS:{

            AstFieldAccess * fAccess = (AstFieldAccess *) node;

            // Compile object to get field from
            compileNode(fAccess->object, program, currentFunction);

            // Find constant index of field name
            Constant fieldName;
            fieldName.tag = STRING_CONST;
            fieldName.string = fAccess->field;
            u16 constIndex = addConstant(program, fieldName);

            addByte(currentFunction, GET_FIELD_OP);
            addU16(currentFunction, constIndex);

            break;
        };
        case AST_FIELD_ASSIGNMENT:{

            AstFieldAssignment * assignment = (AstFieldAssignment *) node;

            // Compile object to get field from
            compileNode(assignment->object, program, currentFunction);

            // Compile value
            compileNode(assignment->value, program, currentFunction);

            // Find constant index of field name
            Constant fieldName;
            fieldName.tag = STRING_CONST;
            fieldName.string = assignment->field;
            u16 constIndex = addConstant(program, fieldName);

            addByte(currentFunction, SET_FIELD_OP);
            addU16(currentFunction, constIndex);

            break;

        };
        case AST_FUNCTION_CALL:{

            AstFunctionCall * call = (AstFunctionCall *) node;

            // Compile what function should be called
            compileNode(call->function, program, currentFunction);

            // Compile arguments
            for(size_t i = 0; i < call->argument_cnt; ++i){
                compileNode(call->arguments[i], program, currentFunction);
            }

            // Add call function operation
            addByte(currentFunction, CALL_FUNCTION_OP);
            addByte(currentFunction, call->argument_cnt);

            break;
        };
        case AST_METHOD_CALL:{

            AstMethodCall * call = (AstMethodCall *) node;

            compileNode(call->object, program, currentFunction);

            for(size_t i = 0; i < call->argument_cnt; ++i){
                compileNode(call->arguments[i], program, currentFunction);
            }

            Constant fieldName;
            fieldName.tag = STRING_CONST;
            fieldName.string = call->name;
            u16 constIndex = addConstant(program, fieldName);

            addByte(currentFunction, CALL_METHOD_OP);
            addU16(currentFunction, constIndex);
            addByte(currentFunction, call->argument_cnt + 1);

            break;
        };
        case AST_CONDITIONAL:{

            AstConditional * conditional = (AstConditional *) node;

            // Compile condition
            compileNode(conditional->condition, program, currentFunction);

            // Add branch operation

            addByte(currentFunction, BRANCH_OP);
            size_t byteIndexToBranch = currentFunction->body->size;
            addU16(currentFunction, 0);
            size_t sizeBeforeFalseBranch = currentFunction->body->size;

            // False branch should be right after Branch
            // Enter block
            size_t localCounter = currentFunction->locals->size;
            bool isMain = currentFunction->isMain;
            currentFunction->isMain = false;

            compileNode(conditional->alternative, program, currentFunction);

            // Exit block
            currentFunction->isMain = isMain;
            restoreLocals(currentFunction->locals, localCounter);


            // Jump over true case
            addByte(currentFunction, JUMP_OP);


            size_t byteIndexToJump = currentFunction->body->size;
            addU16(currentFunction, 0);
            size_t sizeAfterFalseBranch = currentFunction->body->size;

            // Compile true case

            // Enter block
            localCounter = currentFunction->locals->size;
            isMain = currentFunction->isMain;
            currentFunction->isMain = false;

            compileNode(conditional->consequent, program, currentFunction);

            // Exit block
            currentFunction->isMain = isMain;
            restoreLocals(currentFunction->locals, localCounter);


            size_t sizeAfterTrueBranch = currentFunction->body->size;

            i16 trueCaseOffset = (i16)(((i32)sizeAfterFalseBranch - sizeBeforeFalseBranch));
            i16 afterCondOffset = (i16)(((i32)sizeAfterTrueBranch - sizeAfterFalseBranch));

            // Complete offsets

            insert_i16(currentFunction->body, byteIndexToBranch, trueCaseOffset);
            insert_i16(currentFunction->body, byteIndexToJump, afterCondOffset);

            break;
        };
        case AST_LOOP:{
            AstLoop * loop = (AstLoop *) node;

            size_t sizeBeforeCondition = currentFunction->body->size;

            // Compile condition
            compileNode(loop->condition, program, currentFunction);

            // If condition is false add jump over jump to true case
            addByte(currentFunction, BRANCH_OP);
            addU16(currentFunction, 3);
            addByte(currentFunction, JUMP_OP);

            size_t indexToJump = currentFunction->body->size;
            addU16(currentFunction, 0);
            size_t sizeBeforeTrueBranch = currentFunction->body->size;

            // Add true branch
            // Enter block
            size_t localCounter = currentFunction->locals->size;
            bool isMain = currentFunction->isMain;
            currentFunction->isMain = false;
            compileNode(loop->body, program, currentFunction);
            // Exit block
            currentFunction->isMain = isMain;
            restoreLocals(currentFunction->locals, localCounter);

            // Drop last value
            addByte(currentFunction, DROP_OP);

            addByte(currentFunction, JUMP_OP);
            size_t indexToJump2 = currentFunction->body->size;
            addU16(currentFunction, 0);
            size_t sizeAfterTrueCase = currentFunction->body->size;
            
            // After loop add reference to null
            Constant null;
            null.tag = NULL_CONST;
            addConstantOp(program, currentFunction, null);

            // Complete offsets
            u16 afterIfOffset = sizeAfterTrueCase - sizeBeforeTrueBranch;
            i16 conditionOffset = (i16)((i32) sizeBeforeCondition - sizeAfterTrueCase);

            insert_u16(currentFunction->body, indexToJump, afterIfOffset);
            insert_i16(currentFunction->body, indexToJump2, conditionOffset);

            break;
        };
        case AST_PRINT:{

            AstPrint * print = (AstPrint *) node;

            // Add format to const pool
            Constant format;
            format.tag = STRING_CONST;
            format.string = print->format;

            size_t constIndex = addConstant(program, format);

            // Compile all arguments
            for(size_t i = 0; i < print->argument_cnt; ++i){
                compileNode(print->arguments[i], program, currentFunction);
            }

            // Add print operation
            addByte(currentFunction, PRINT_OP);
            addU16(currentFunction, constIndex); // Index to const pool
            addByte(currentFunction, print->argument_cnt);// Number of args

            break;
        }
        case AST_BLOCK:{
            AstBlock * block = (AstBlock *) node;

            // Take note what locals are in current scope so far
            size_t localCounter = currentFunction->locals->size;
            bool isMain = currentFunction->isMain;

            for(size_t i = 0; i < block->expression_cnt; ++i){

                currentFunction->isMain = false;

                compileNode(block->expressions[i], program, currentFunction);

                // Put drop after expression if it is not last expression which is return value of the block
                if(i < block->expression_cnt - 1){
                    addByte(currentFunction, DROP_OP);
                }
            }
            if(block->expression_cnt == 0){
                Constant nullConstant;
                nullConstant.tag = NULL_CONST;

                addConstantOp(program, currentFunction, nullConstant);
            }

            currentFunction->isMain = isMain;

            // Reset local counter
            restoreLocals(currentFunction->locals, localCounter);

            break;
        }
        case AST_TOP:{

            AstTop * top = (AstTop *) node;

            // Create main
            FunctionConst mainFunc;
            mainFunc.localCount = 0;
            mainFunc.paramCount = 1;
            mainFunc.locals = localMap_alloc();
            mainFunc.body = createVector();
            mainFunc.isMain = true;
            mainFunc.objectConstructionInProgress = false;

            defineVar(&mainFunc, STR("this"));

            // Compile all sub expressions
            for(size_t i = 0; i < top->expression_cnt; ++i){
                compileNode(top->expressions[i], program, &mainFunc);

                // Put drop after expression if it is not last expression which is return value of the block
                if(i < top->expression_cnt - 1){
                    addByte(&mainFunc, DROP_OP);
                }
            }

            // Add return to main
            addByte(&mainFunc, RETURN_OP);

            // Add main to const pool
            Constant mainFuncConst;
            mainFuncConst.tag = FUNCTION_CONST;
            mainFuncConst.function = mainFunc;

            size_t entryPoint = addConstant(program, mainFuncConst);

            // Set entry point
            program->entryPoint = entryPoint;
            break;
        }

    }
}

void serializeConstant(Constant constant, FILE * out){

    ByteVector * buffer = createVector();

    push_back_byte(buffer, constant.tag);

    switch (constant.tag) {
        case INTEGER_CONST:{
            push_back_i32(buffer, constant.integer);
            break;
        }
        case NULL_CONST:
            break;
        case STRING_CONST:{

            push_back_u32(buffer, constant.string.len);

            for(size_t i = 0; i < constant.string.len; ++i){
                push_back_byte(buffer, constant.string.str[i]);
            }
            break;
        }

        case FUNCTION_CONST:{

            push_back_byte(buffer, constant.function.paramCount);

            push_back_u16(buffer, constant.function.localCount - constant.function.paramCount );

            push_back_u32(buffer, constant.function.body->size);

            for(size_t i = 0; i < constant.function.body->size; ++i){
                push_back_byte(buffer, constant.function.body->bytes[i]);
            }
            break;
        }
        case BOOLEAN_CONST:{
            push_back_byte(buffer, constant.boolean);
            break;
        }
        case CLASS_CONST:{

            push_back_u16(buffer, constant.class.fieldMap->size);

            for(size_t i = 0; i < constant.class.fieldMap->size; ++i){
                u16 toAdd = constant.class.fieldMap->constIndexes[i];
                push_back_u16(buffer, toAdd);
            }
            break;
        }
    }

    if(buffer->bytes != NULL)
        fwrite(buffer->bytes, buffer->size, sizeof(Byte), out);

    destroyVector(buffer);
}

void serialize(Program * program, FILE * out){

    // Create HEADER
    push_back_byte(program->header, 'F');
    push_back_byte(program->header, 'M');
    push_back_byte(program->header, 'L');
    push_back_byte(program->header, '\n');

    // Output header
    if(program->header->bytes != NULL)
        fwrite(program->header->bytes, program->header->size, sizeof(Byte), out);

    // Output constant pool

    // Number of constants
    ByteVector * constantNum = createVector();

    push_back_u16(constantNum, program->constantPoolSize);

    if(constantNum->bytes != NULL)
        fwrite(constantNum->bytes, constantNum->size, sizeof(Byte), out);
    destroyVector(constantNum);

    for(size_t i = 0; i < program->constantPoolSize; ++i){
        Constant constant = program->constantPool[i];
        serializeConstant(constant, out);
    }

    // Output globals

    // Number of globals
    ByteVector * globalNum = createVector();

    push_back_u16(globalNum, program->globalsSize);

    if(globalNum->bytes != NULL)
        fwrite(globalNum->bytes, globalNum->size, sizeof(Byte), out);
    destroyVector(globalNum);

    ByteVector * globals = createVector();
    for(size_t i = 0; i < program->globalsSize; ++i){
        push_back_u16(globals, program->globals[i]);
    }

    if(globals->bytes != NULL)
        fwrite(globals->bytes, globals->size, sizeof(Byte), out);
    destroyVector(globals);

    // Output entry point
    ByteVector * entryPoint = createVector();

    push_back_u16(entryPoint, program->entryPoint);

    if(entryPoint->bytes != NULL)
        fwrite(entryPoint->bytes, entryPoint->size, sizeof(Byte), out);
    destroyVector(entryPoint);

    fflush(out);
//    fclose(out);
}

void bcCompile(Ast * ast, FILE * out){
    Program * program = createProgram();

    compileNode(ast, program, NULL);

    serialize(program, out);

    destroyProgram(program);
}