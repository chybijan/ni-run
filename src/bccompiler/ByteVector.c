//
// Created by jerror on 4/7/23.
//

#include "ByteVector.h"

ByteVector * createVector(){
    ByteVector * vector = (ByteVector *) malloc(sizeof(ByteVector));
    
    vector->size = 0;
    vector->capacity = 0;
    vector->bytes = NULL;
    
    return vector;
}

void destroyVector(ByteVector * vector){
    free(vector->bytes);
    free(vector);
}

void insert_u16(ByteVector * vector, size_t indexToVector, u16 toInsert){
    Byte lowByte = (Byte) (toInsert);
    Byte highByte = (Byte) (toInsert >> 8);

    vector->bytes[indexToVector] = lowByte;
    vector->bytes[indexToVector + 1] = highByte;
}

void insert_i16(ByteVector * vector, size_t indexToVector, i16 toInsert){
    Byte lowByte = (Byte) (toInsert) & 0xFF;
    Byte highByte = (Byte) (toInsert >> 8) & 0xFF;

    vector->bytes[indexToVector] = lowByte;
    vector->bytes[indexToVector + 1] = highByte;
}


Byte * push_back_byte(ByteVector * vector, Byte byte){

    // Allocate new memory if we're out of space for constants
    if(vector->capacity == vector->size){

        vector->capacity = (vector->capacity + 1) * 2;

        Byte * temp = vector->bytes;
        vector->bytes = (Byte *) malloc(vector->capacity * sizeof(Byte));

        // Copy old value to new
        if(temp != NULL)
            memcpy(vector->bytes, temp, vector->size);

        // Free old memory
        free(temp);
    }

    // Add new constant
    vector->bytes[vector->size] = byte;

    Byte * pointerToAllocatedSpace = &vector->bytes[vector->size];

    vector->size += 1;

    return pointerToAllocatedSpace;
}

Byte * push_back_u16(ByteVector * vector, u16 toAdd){
    Byte highByte = (Byte) (toAdd >> 8);
    Byte lowByte = (Byte) toAdd;

    Byte * origin = push_back_byte(vector, lowByte);
    push_back_byte(vector, highByte);

    return origin;
}

Byte * push_back_u32(ByteVector * vector, u32 toAdd){
    Byte byte0 = (Byte) toAdd;
    Byte byte1 = (Byte) (toAdd >> 8);
    Byte byte2 = (Byte) (toAdd >> 16);
    Byte byte3 = (Byte) (toAdd >> 24);

    Byte * origin = push_back_byte(vector, byte0);
    push_back_byte(vector, byte1);
    push_back_byte(vector, byte2);
    push_back_byte(vector, byte3);

    return origin;
}

Byte * push_back_i32(ByteVector * vector, i32 toAdd){
    Byte byte0 = (Byte) ((toAdd) & 0xFF);
    Byte byte1 = (Byte) ((toAdd >> 8) & 0xFF);
    Byte byte2 = (Byte) ((toAdd >> 16) & 0xFF);
    Byte byte3 = (Byte) ((toAdd >> 24) & 0xFF);

    Byte * origin = push_back_byte(vector, byte0);
    push_back_byte(vector, byte1);
    push_back_byte(vector, byte2);
    push_back_byte(vector, byte3);

    return origin;
}
