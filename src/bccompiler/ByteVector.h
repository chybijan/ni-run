//
// Created by jerror on 4/7/23.
//

#ifndef RUN_BYTEVECTOR_H
#define RUN_BYTEVECTOR_H

#include "src/bccompiler/Program.h"

typedef unsigned char Byte;


ByteVector * createVector();
void destroyVector(ByteVector * vector);

void insert_u16(ByteVector * vector, size_t indexToVector, u16 toInsert);
void insert_i16(ByteVector * vector, size_t placeToInsert, i16 toInsert);

Byte * push_back_u16(ByteVector * vector, u16 toAdd);
Byte * push_back_u32(ByteVector * vector, u32 toAdd);
Byte * push_back_i32(ByteVector * vector, i32 toAdd);

Byte * push_back_byte(ByteVector * vector, Byte byte);
Byte * push_back_u16(ByteVector * vector, u16 toAdd);
Byte * push_back_u32(ByteVector * vector, u32 toAdd);
Byte * push_back_i32(ByteVector * vector, i32 toAdd);



#endif //RUN_BYTEVECTOR_H
