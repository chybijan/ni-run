//
// Created by jerror on 4/7/23.
//

#include "Locals.h"
#include "Program.h"


FieldMap * fieldMap_alloc(){
    FieldMap * map = (FieldMap *) malloc(sizeof(FieldMap));

    map->size = 0;
    map->capacity = 0;
    map->fieldNames = NULL;
    map->constIndexes = NULL;

    return map;
}
void fieldMap_destroy(FieldMap * map){

    free(map->fieldNames);
    free(map->constIndexes);
    free(map);
}

size_t addField(FieldMap * map, Str localName, u16 constantIndex){

    if(map->size == map->capacity){

        // Alloc new places
        size_t newCapacity = (map->capacity + 1) * 2;

        FieldMap * newMap = fieldMap_alloc();

        newMap->fieldNames = (Str *) malloc(newCapacity * sizeof(Str));
        newMap->constIndexes = (u16 *) malloc(newCapacity * sizeof(u16));

        newMap->capacity = newCapacity;

        // Copy old indexes into new ones
        for(size_t i = 0; i < map->capacity; ++i){
            newMap->fieldNames[i] = map->fieldNames[i];
            newMap->constIndexes[i] = map->constIndexes[i];
        }

        // Copy indexes to old map and destroy new tmp map
        Str * tmp1 = map->fieldNames;
        u16 * tmp2 = map->constIndexes;


        map->capacity = newCapacity;
        map->fieldNames = newMap->fieldNames;
        map->constIndexes = newMap->constIndexes;

        newMap->fieldNames = tmp1;
        newMap->constIndexes = tmp2;

        fieldMap_destroy(newMap);
    }

    // Add new element
    map->fieldNames[map->size] = localName;
    map->constIndexes[map->size] = constantIndex;

    map->size++;

    return map->size - 1;
}

int findFieldInMap(FieldMap * map, Str localName){
    // Find from the end, because old local could be overridden
    for(int i = (int) map->size - 1; i >= 0; --i){
        Str mapKey = map->fieldNames[i];
        if(str_eq(mapKey, localName)){
            return i;
        }
    }

    return -1;
}


LocalMap * localMap_alloc(){
    LocalMap * map = (LocalMap *) malloc(sizeof(LocalMap));
    
    map->size = 0;
    map->capacity = 0;
    map->keys = NULL;
    
    return map;
}


void localMap_destroy(LocalMap * map){

    free(map->keys);
    free(map);

}

void localMap_add(LocalMap * map, Str key){
    if(map->size == map->capacity){

        // Alloc new places
        size_t newCapacity = (map->capacity + 1) * 2;

        LocalMap * newMap = localMap_alloc();

        newMap->keys = (Str *) malloc(newCapacity * sizeof(Str));
        newMap->capacity = newCapacity;

        // Copy old indexes into new ones
        for(size_t i = 0; i < map->capacity; ++i){
            newMap->keys[i] = map->keys[i];
        }

        // Copy indexes to old map and destroy new tmp map
        Str * tmpIndexes = map->keys;

        map->capacity = newCapacity;
        map->keys = newMap->keys;

        newMap->keys = tmpIndexes;

        localMap_destroy(newMap);
    }

    // Add new element
    map->keys[map->size] = key;
    map->size++;
}

int findLocal(LocalMap * map, Str key){

    // Find from the end, because old local could be overridden
    for(int i = (int) map->size - 1; i >= 0; --i){
        Str mapKey = map->keys[i];
        if(str_eq(mapKey, key)){
            return i;
        }
    }

    return -1;
}

size_t addLocal(LocalMap * locals, Str localName){

//    int localIndex = findLocal(locals, localName);
//    if(localIndex != -1){
//        return localIndex;
//    }

    // Always create new local
    localMap_add(locals, localName);
    return locals->size - 1;

}


void restoreLocals(LocalMap * locals, size_t numberOfLocalsToKeep){
    locals->size = numberOfLocalsToKeep;
}