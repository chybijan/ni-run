//
// Created by jerror on 4/7/23.
//

#ifndef RUN_LOCALS_H
#define RUN_LOCALS_H

#include "parser.h"


typedef struct LocalMap{

    Str * keys;

    size_t size;
    size_t capacity;

} LocalMap;

typedef struct{
    Str * fieldNames;
    u16 * constIndexes;

    size_t size;
    size_t capacity;
} FieldMap;

FieldMap * fieldMap_alloc();
void fieldMap_destroy(FieldMap * map);

size_t addField(FieldMap * locals, Str localName, u16 constantIndex);
int findField(FieldMap * locals, Str localName);


LocalMap * localMap_alloc();
void localMap_destroy(LocalMap * map);
//
//void localMap_update(LocalMap * map, Str key, size_t value);
//void localMap_put(LocalMap * map, Str key, size_t value);
//size_t * localMap_find(LocalMap * map, Str key);

// Create or find local in current scope. Return its number in the current frame
size_t addLocal(LocalMap * locals, Str localName);
int findLocal(LocalMap * locals, Str localName);

void restoreLocals(LocalMap * locals, size_t numberOfLocalsToKeep);


#endif //RUN_LOCALS_H
