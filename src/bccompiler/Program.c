//
// Created by jerror on 4/4/23.
//

#include "Program.h"
#include "ByteVector.h"

Program * createProgram(){

    Program * program = (Program *)malloc(sizeof(Program));

    program->constantPool = NULL;
    program->constantPoolSize = 0;
    program->constantPoolCapacity = 0;
    
    program->globals = NULL;
    program->globalsSize = 0;
    program->globalsCapacity = 0;

    program->header = createVector();

    return program;

}

void destroyProgram(Program * program){

    // Destroy all constant with allocated memory
    for(size_t i = 0; i < program->constantPoolSize; ++i){
        Constant constant = program->constantPool[i];
        if(constant.tag == FUNCTION_CONST){
            localMap_destroy(constant.function.locals);
            destroyVector(constant.function.body);
        }else if(constant.tag == CLASS_CONST){
            fieldMap_destroy(constant.class.fieldMap);
        }
    }

    free(program->constantPool);
    free(program->globals);

    destroyVector(program->header);
    free(program);
}

// return index of the constant in constant pool
size_t addConstant(Program * program, Constant constant){

    // Try to find constant
    for(size_t i = 0; i < program->constantPoolSize; ++i){
        Constant currentConst = program->constantPool[i];

        if(constant.tag == currentConst.tag){
            switch (constant.tag) {
                case INTEGER_CONST:{
                    if(constant.integer == currentConst.integer)
                        return i;
                    break;
                }
                case NULL_CONST:{
                    return i;
                }
                case STRING_CONST:{
                    if(str_eq(constant.string, currentConst.string)){
                        return i;
                    }
                    break;

                }
                case BOOLEAN_CONST: {
                    if(constant.boolean == currentConst.boolean){
                        return i;
                    }
                    break;

                }
                case FUNCTION_CONST:
                case CLASS_CONST:
                    break;
            }
        }
    }

    // Constant not found so create new
    
    // Allocate new memory if we're out of space for constants
    if(program->constantPoolCapacity == program->constantPoolSize){

        program->constantPoolCapacity = (program->constantPoolCapacity + 1) * 2;
        
        Constant * temp = program->constantPool;
        program->constantPool = (Constant *) malloc( program->constantPoolCapacity * sizeof(Constant));
        
        // Copy old value to new
        if(temp != NULL)
            memcpy(program->constantPool, temp, program->constantPoolSize * sizeof(Constant));
        
        // Free old memory
        free(temp);
    }
    
    // Add new constant
    program->constantPool[program->constantPoolSize] = constant;

    size_t thisIndex = program->constantPoolSize;

    program->constantPoolSize += 1;

    return thisIndex;
}

// Return index to const pool with name of the global var
size_t addGlobal(Program * program, Str globalName){

    Constant globalNameConst;
    globalNameConst.tag = STRING_CONST;
    globalNameConst.string = globalName;

    size_t constIndex = addConstant(program, globalNameConst);

    // Find if the global is already present
    for(size_t i = 0; i < program->globalsSize; ++i){
        size_t constIndexOfTheGlobal = program->globals[i];
        if(constIndexOfTheGlobal == constIndex)
            return constIndexOfTheGlobal;
    }

    // Global is not yet added
    // Allocate new memory if we're out of space for globals
    if(program->globalsCapacity == program->globalsSize){

        program->globalsCapacity = (program->globalsCapacity + 1) * 2;

        size_t * temp = program->globals;
        program->globals = (size_t *) malloc( program->globalsCapacity * sizeof(size_t));

        // Copy old value to new
        if(temp != NULL)
            memcpy(program->globals, temp, program->globalsSize * sizeof(size_t));

        // Free old memory
        free(temp);
    }

    // Add new constant
    program->globals[program->globalsSize] = constIndex;

//    size_t thisIndex = program->globalsSize;

    program->globalsSize += 1;

    return constIndex;
}
