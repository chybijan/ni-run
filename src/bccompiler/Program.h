//
// Created by jerror on 4/4/23.
//

#ifndef RUN_PROGRAM_H
#define RUN_PROGRAM_H

#include <stddef.h>
#include "reference/parser.h"
#include "Locals.h"

typedef unsigned char Byte;

typedef struct ByteVector{
    Byte * bytes;
    size_t size;
    size_t capacity;
} ByteVector;

typedef enum{
    CONSTANT_OP = 0x01,
    DROP_OP = 0x00,
    PRINT_OP = 0x02,
    RETURN_OP = 0x0F,
    GET_LOCAL_OP = 0x0A,
    SET_LOCAL_OP = 0x09,
    GET_GLOBAL_OP = 0x0C,
    SET_GLOBAL_OP = 0x0B,
    JUMP_OP = 0x0E,
    BRANCH_OP = 0x0D,
    CALL_FUNCTION_OP = 0x08,
    ARRAY_OP = 0x03,
    OBJECT_OP = 0x04,
    GET_FIELD_OP = 0x05,
    SET_FIELD_OP = 0x06,
    CALL_METHOD_OP = 0x07,
} OpCode;

typedef enum{
    INTEGER_CONST   = 0x00,
    NULL_CONST      = 0x01,
    STRING_CONST    = 0x02,
    FUNCTION_CONST  = 0x03,
    BOOLEAN_CONST   = 0x04,
    CLASS_CONST     = 0x05,
} ConstantTag;

typedef struct {
    FieldMap * fieldMap;
} ClassConst;

typedef struct{
    u8 paramCount;
    u8 localCount;

    LocalMap * locals;
    ByteVector * body;
    bool isMain;

    bool objectConstructionInProgress;
    ClassConst * classConstructed;

} FunctionConst;


typedef struct{

    ConstantTag tag;

    union {
        bool boolean;
        i32 integer;
        Str string;
        FunctionConst function;
        ClassConst class;
    };

} Constant;

typedef struct{

    Constant * constantPool;
    size_t constantPoolSize;
    size_t constantPoolCapacity;

    size_t * globals;
    size_t globalsSize;
    size_t globalsCapacity;

    ByteVector * header;

    size_t entryPoint;

} Program;

Program * createProgram();
void destroyProgram(Program * program);

size_t addConstant(Program * program, Constant constant);
size_t addGlobal(Program * program, Str constantPoolIndex);
#endif //RUN_PROGRAM_H
