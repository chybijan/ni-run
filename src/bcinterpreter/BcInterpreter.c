//
// Created by jerror on 3/25/23.
//

#include <stdio.h>
#include <assert.h>
#include "BcInterpreter.h"
#include "FrameStack.h"
#include "OperandStack.h"
#include "src/Heap.h"

#define UNDEF_ENTRY_POINT UINT16_MAX

typedef enum{
    CONSTANT_OP = 0x01,
    DROP_OP = 0x00,
    PRINT_OP = 0x02,
    RETURN_OP = 0x0F,
    GET_LOCAL_OP = 0x0A,
    SET_LOCAL_OP = 0x09,
    GET_GLOBAL_OP = 0x0C,
    SET_GLOBAL_OP = 0x0B,
    JUMP_OP = 0x0E,
    BRANCH_OP = 0x0D,
    CALL_FUNCTION_OP = 0x08,
    ARRAY_OP = 0x03,
    OBJECT_OP = 0x04,
    GET_FIELD_OP = 0x05,
    SET_FIELD_OP = 0x06,
    CALL_METHOD_OP = 0x07,
} OpCode;

typedef struct{
    u8 paramCount;
    u16 localCount;
    size_t instructionCount;
    Instruction functionStart;
} FunctionConst;

typedef struct {
    u16 memberCount;
    u16 * members;
} ClassConst;

typedef enum{
    INTEGER_CONST   = 0x00,
    NULL_CONST      = 0x01,
    STRING_CONST    = 0x02,
    FUNCTION_CONST  = 0x03,
    BOOLEAN_CONST   = 0x04,
    CLASS_CONST     = 0x05,
} ConstantTag;

typedef struct{

    ConstantTag tag;

    union {
        bool boolean;
        i32 integer;
        Str string;
        FunctionConst function;
        ClassConst class;
    };

} Constant;

typedef struct VM{
    Heap * heap;
    FrameStack * frameStack;
    OperandStack * operandStack;
    Constant * constantPool;
    size_t constantPoolSize;

    Instruction currentInstruction;
    u16 entryPoint;
    BcObjectRuntimeObject * globalClass;
} VM;

bool bcIsTruth(Pointer pointer) {
    RuntimeObject *runtimeObject = (RuntimeObject *) pointer;
    if (runtimeObject->type == TYPE_BOOLEAN) {
        return ((BooleanRuntimeObject *) runtimeObject)->value == true;
    } else if (runtimeObject->type == TYPE_NULL) {
        return false;
    }
    return true;
}

VM * initialize_VM(Instruction program, size_t heapSize, size_t operandStackSize){

    // Initialize heap
    Heap * heap = heap_alloc(heapSize);

    // Initialize Frame stack
    FrameStack * frameStack = frameStack_alloc();

    // Initialize Operand stack
    OperandStack * operandStack = operandStack_alloc(operandStackSize);

    VM * vm = (VM *) malloc(sizeof(VM));
    vm->heap = heap;
    vm->frameStack = frameStack,
    vm->operandStack = operandStack;
    vm->constantPool = NULL;

    vm->currentInstruction = program;
    vm->entryPoint = UNDEF_ENTRY_POINT;

    return vm;
}

void initializeConstantPool(VM * vm, size_t constantPoolSize){
    Constant * constantPool = (Constant *) malloc(constantPoolSize * sizeof(Constant));

    vm->constantPoolSize = constantPoolSize;
    vm->constantPool = constantPool;
}

void destroy_vm(VM * vm){
    heap_destroy(vm->heap);
    frameStack_destroy(vm->frameStack);
    operandStack_destroy(vm->operandStack);

    for(size_t i = 0; i < vm->constantPoolSize; ++i){
        Constant c = vm->constantPool[i];
        if(c.tag == CLASS_CONST){
            free(c.class.members);
        }
    }

    if(vm->constantPool != NULL){
        free(vm->constantPool);
    }

    free(vm);
}

BcByte fetchByte(VM * vm){
    BcByte b = *vm->currentInstruction;
    vm->currentInstruction += 1;
    return b;
}

u32 fetchNumber(size_t numberOfBytesEncodingNumber, VM * vm){
    size_t finalNumber = 0;

    for(size_t i = 0; i < numberOfBytesEncodingNumber; ++i){
        size_t byteNumber = (size_t) fetchByte(vm);
        finalNumber |= byteNumber << (i*8);
    }

    return finalNumber;
}

void readHeader(VM * vm){

    BcByte first = fetchByte(vm);
    BcByte second = fetchByte(vm);
    BcByte third = fetchByte(vm);
    BcByte fourth = fetchByte(vm);

    assert(first == 'F');
    assert(second == 'M');
    assert(third == 'L');
    assert(fourth == '\n');

}

void loadConstant(size_t constantIndex, VM * vm) {

    ConstantTag constantTag = fetchByte(vm);

    Constant * constant = vm->constantPool + constantIndex;
    constant->tag = constantTag;

    switch (constantTag) {
        case INTEGER_CONST:{
            i32 intValue = (i32) fetchNumber(4, vm);
            constant->integer = intValue;
            break;
        }
        case NULL_CONST:
            break;
        case STRING_CONST:{
            size_t stringLength = (size_t) fetchNumber(4, vm);
            constant->string.len = stringLength;
            constant->string.str = vm->currentInstruction;

            vm->currentInstruction += stringLength;
            break;
        }
        case FUNCTION_CONST:
            constant->function.paramCount = fetchByte(vm);
            constant->function.localCount = fetchNumber(2, vm);
            constant->function.instructionCount = fetchNumber(4, vm);
            constant->function.functionStart = vm->currentInstruction;

            vm->currentInstruction += constant->function.instructionCount;
            break;
        case BOOLEAN_CONST:{
            bool boolValue = fetchByte(vm) == 0x01;
            constant->boolean = boolValue;
            break;
        }
        case CLASS_CONST:{

            u16 memberCount = fetchNumber(2, vm);

            constant->class.memberCount = memberCount;
            constant->class.members = (u16 *) malloc(memberCount * sizeof(u16));

            for(int i = 0; i < memberCount; ++i){
                constant->class.members[i] = fetchNumber(2, vm);
            }
            break;
        }

    }

}

void loadConstants(VM * vm){

    size_t numberOfConstants = fetchNumber(2, vm);

    initializeConstantPool(vm, numberOfConstants);

    for(size_t i = 0; i < numberOfConstants; ++i){
        loadConstant(i, vm);
    }

}

void loadEntryPoint(VM * vm){
    u16 entryPoint = fetchNumber(2, vm);
    vm->entryPoint = entryPoint;
}

void executeFetchDecodeExecuteLoop(VM * vm);


void callFunction(u16 functionConstantIndex, Pointer * arguments, size_t numberOfArguments, VM * vm){
    // This always has to be included
    assert(numberOfArguments > 0);

    Constant function = vm->constantPool[functionConstantIndex];

    assert(function.tag == FUNCTION_CONST);
    FunctionConst functionConst = function.function;

    assert(functionConst.paramCount == numberOfArguments);

    // Create function frame
    size_t numberOfLocals = functionConst.localCount + functionConst.paramCount;
    frameStack_create_frame(vm->frameStack, vm->currentInstruction, numberOfLocals);

    // Populate frame with arguments
    addLocalToFrame(0, arguments[0], vm->frameStack);
    for(size_t i = 1; i < functionConst.paramCount; ++i){
        addLocalToFrame(i, arguments[i], vm->frameStack);
    }

    // Populate rest of the frame with nulls
    for(size_t i = functionConst.paramCount; i < numberOfLocals; ++i){
        addLocalToFrame(i, allocate_null(vm->heap), vm->frameStack);
    }

    // Set return address
    setReturnAddress(vm->currentInstruction, vm->frameStack);

    vm->currentInstruction = functionConst.functionStart;

    executeFetchDecodeExecuteLoop(vm);
}

void callPrimitive(Str methodName, RuntimeObject * runtimeObject, Pointer * arguments, size_t numberOfArguments, VM * vm){
    // Call array methods
    if (runtimeObject->type == TYPE_ARRAY) {
        assert(numberOfArguments >= 1);

        BcArrayRuntimeObject * array = (BcArrayRuntimeObject *) runtimeObject;
        IntegerRuntimeObject * index = (IntegerRuntimeObject *) arguments[0];

        if (str_eq(methodName, STR("get"))) {
            Pointer val = array->elements[index->value];
            operandStack_push(val, vm->operandStack);
            return;
        } else if (str_eq(methodName, STR("set"))) {
            assert(numberOfArguments == 2);

            array->elements[index->value] = arguments[1];
            operandStack_push(arguments[1], vm->operandStack);
            return;
        }
    }

    // Call builtin functions
    if (numberOfArguments != 1) {
        printf("Invalid number of arguments\n");
        exit(1);
    }

    Pointer rightSide = arguments[0];
    Pointer val = NULL;

    // Operators defined on multiple types
    if(str_eq(methodName, STR("=="))){
        if(runtimeObject->type != ((RuntimeObject *)rightSide)->type){
            val = allocate_boolean(false, vm->heap);
        }

        // Types are equal
        else if(runtimeObject->type == TYPE_NULL){
            val = allocate_boolean(true, vm->heap);
        }
        else if(runtimeObject->type == TYPE_INTEGER){
            val = allocate_boolean(((IntegerRuntimeObject *) runtimeObject)->value == ((IntegerRuntimeObject *)rightSide)->value, vm->heap);
        }else if(runtimeObject->type == TYPE_BOOLEAN){
            val = allocate_boolean(((BooleanRuntimeObject *)runtimeObject)->value == ((BooleanRuntimeObject *)rightSide)->value, vm->heap);
        }

    }else if(str_eq(methodName, STR("!="))){
        if(runtimeObject->type != ((RuntimeObject *)rightSide)->type){
            val = allocate_boolean(true, vm->heap);
        }

        // Types are equal
        else if(runtimeObject->type == TYPE_NULL){
            val = allocate_boolean(false, vm->heap);
        }else if(runtimeObject->type == TYPE_INTEGER){
            val = allocate_boolean(((IntegerRuntimeObject *) runtimeObject)->value != ((IntegerRuntimeObject *)rightSide)->value, vm->heap);
        }else if(runtimeObject->type == TYPE_BOOLEAN){
            val = allocate_boolean(((BooleanRuntimeObject *)runtimeObject)->value != ((BooleanRuntimeObject *)rightSide)->value, vm->heap);
        }
    }

    else if (runtimeObject->type == TYPE_INTEGER) {
        IntegerRuntimeObject *leftInt = (IntegerRuntimeObject *) runtimeObject;
        IntegerRuntimeObject *rightInt = ((IntegerRuntimeObject *)rightSide);
        if (str_eq(methodName, STR("+"))) {
            val = allocate_integer(leftInt->value + rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR("-"))) {
            val = allocate_integer(leftInt->value - rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR("*"))) {
            val = allocate_integer(leftInt->value * rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR("/"))) {
            val = allocate_integer(leftInt->value / rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR("%"))) {
            val = allocate_integer(leftInt->value % rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR("<="))) {
            val = allocate_boolean(leftInt->value <= rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR(">="))) {
            val = allocate_boolean(leftInt->value >= rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR("<"))) {
            val = allocate_boolean(leftInt->value < rightInt->value, vm->heap);
        } else if (str_eq(methodName, STR(">"))) {
            val = allocate_boolean(leftInt->value > rightInt->value, vm->heap);
        }
    } else if (runtimeObject->type == TYPE_BOOLEAN) {
        BooleanRuntimeObject *leftBool = ((BooleanRuntimeObject *)runtimeObject);
        BooleanRuntimeObject *rightBool = ((BooleanRuntimeObject *)rightSide);
        if (str_eq(methodName, STR("&"))) {
            val = allocate_boolean(leftBool->value & rightBool->value, vm->heap);
        } else if (str_eq(methodName, STR("|"))) {
            val = allocate_boolean(leftBool->value | rightBool->value, vm->heap);
        }
    }

    if(val == NULL){
//        RuntimeObject * r= (RuntimeObject *)
        printf("Method not recognized\n");
        exit(1);
    }else{
        // Push result to operand stack
        operandStack_push(val, vm->operandStack);
    }

}

void bcCallMethod(Str methodName, Pointer receiverPtr, Pointer * arguments, size_t numberOfArguments, VM * vm){

    RuntimeObject * runtimeObject = (RuntimeObject *) receiverPtr;
    if(runtimeObject->type == TYPE_OBJECT){
        BcObjectRuntimeObject * receiver = (BcObjectRuntimeObject *) receiverPtr;

        BcField * field = findBcField(receiver, methodName);
        if(field == NULL){
            // Method is not in receiver

            Pointer parent = receiver->parent;
            if(parent != NULL){
                // try to find it in parent
                bcCallMethod(methodName, parent, arguments, numberOfArguments, vm);
                return;
            }

            printf("Method does not exist in object hierarchy\n");
            exit(1);
        }else{
            // Method is present in the receiver call it
            Pointer functionPtr = field->value;

            RuntimeObject * functionObj = (RuntimeObject *) functionPtr;
            assert(functionObj->type == TYPE_FUNCTION);

            BcFunctionRuntimeValue * function = (BcFunctionRuntimeValue *) functionPtr;

            // Add receiver as first argument to the function
            Pointer * argumentsWithThis = (Pointer *) malloc((numberOfArguments + 1) * sizeof(Pointer));

            argumentsWithThis[0] = receiverPtr;
            for(size_t i = 1; i < numberOfArguments + 1; ++i){
                argumentsWithThis[i] = arguments[i-1];
            }

            callFunction(function->indexToConstPool, argumentsWithThis, numberOfArguments + 1, vm);

            free(argumentsWithThis);
            return;
        }
    }

    // Receiver is not an object, so it must be one of primitives
    callPrimitive(methodName, runtimeObject, arguments, numberOfArguments, vm);
    
}

int fieldCmp (const void * a, const void * b) {
    BcField * fieldA = (BcField *) a;
    BcField * fieldB = (BcField *) b;

    return str_cmp(fieldA->name,  fieldB->name);
}

void printValue(Pointer valuePtr) {

    Value valueToPrint = {valuePtr};

    RuntimeObject *runtimeObject = convertPtrToRuntimeObject(valueToPrint);

    switch (runtimeObject->type) {
        case TYPE_NULL:
            printf("%s", "null");
            break;
        case TYPE_BOOLEAN: {
            BooleanRuntimeObject *booleanRuntimeObject = asBool(valueToPrint);
            printf("%s", booleanRuntimeObject->value ? "true" : "false");
            break;
        }
        case TYPE_INTEGER: {
            IntegerRuntimeObject *integerRuntimeObject = asInt(valueToPrint);
            printf("%d", integerRuntimeObject->value);
            break;
        }
        case TYPE_ARRAY: {
            printf("[");
            ArrayRuntimeObject * array = asArray(valueToPrint);
            for (size_t i = 0; i < array->length; i++) {
                if (i != 0) {
                    printf(", ");
                }
                printValue(array->elements[i].valueInHeap);
            }
            printf("]");
            break;
        }
        case TYPE_OBJECT: {
            printf("object(");
            BcObjectRuntimeObject * objectRuntimeObject = (BcObjectRuntimeObject *) runtimeObject;

            Value parent = {objectRuntimeObject->parent};
            RuntimeObject * parentObject = convertPtrToRuntimeObject(parent);

            bool printComma = false;
            if ( parentObject->type != TYPE_NULL) {
                printf("..=");
                printValue(parent.valueInHeap);
                printComma = true;
            }

            BcField * fields = (BcField *) malloc(objectRuntimeObject->fieldCount * sizeof(BcField));
            // Populate field
            for(size_t i = 0; i < objectRuntimeObject->fieldCount; ++i){
                fields[i] = objectRuntimeObject->fields[i];
            }

            // Sort fields in lexicographical order
            qsort(fields, objectRuntimeObject->fieldCount, sizeof(BcField), fieldCmp);

            for (size_t i = 0; i < objectRuntimeObject->fieldCount; i++) {
                if (printComma) {
                    printf(", ");
                }

                BcField field = fields[i];
                Str name = field.name;
                printf("%.*s=", (int)name.len, name.str);
                printValue(field.value);
                printComma = true;
            }
            printf(")");

            free(fields);

            break;
        }
        case TYPE_FUNCTION:
            printf("function");
            break;
    }
}

void executePrint(Str format, Pointer * arguments, size_t argumentCount) {

    bool in_escape = false;
    size_t arg_index = 0;

    for (size_t i = 0; i < format.len; ++i) {
        u8 c = format.str[i];
        u8 outputChar = c;
        if (in_escape) {
            in_escape = false;
            switch (c) {
                case 'n':
                    outputChar = '\n';
                    break;
                case 't':
                    outputChar = '\t';
                    break;
                case 'r':
                    outputChar = '\r';
                    break;
                case '~':
                    outputChar = '~';
                    break;
                case '"':
                    outputChar = '"';
                    break;
                case '\\':
                    outputChar = '\\';
                    break;
                default: {
                    printf("\n");
                    printf("ESCAPE SEQUENCE NOT RECOGNIZED\n");
                    exit(1);
                }
            }
            putchar(outputChar);
        } else {
            switch (c) {
                case '\\':
                    in_escape = true;
                    break;
                case '~':
                    if (arg_index >= argumentCount) {
                        printf("\n");
                        printf("There are too little arguments\n");
                        exit(1);
                    }

                    Value value = {arguments[arg_index]};
                    printValue(value.valueInHeap);
                    arg_index += 1;
                    break;
                default:
                    putchar(outputChar);
            }
        }
    }
    fflush(stdout);
}

BcObjectRuntimeObject * instantiateClass(ClassConst classConst, VM * vm, bool global){

    BcObjectRuntimeObject * runtimeObject = allocate_bc_object(classConst.memberCount, vm->heap);

    Pointer * parentAndMemberValues;
    if(!global)
        parentAndMemberValues = operandStack_popN(classConst.memberCount + 1, vm->operandStack);

    for(size_t i = 0; i < classConst.memberCount; ++i){
        u16 constantPoolIndex = classConst.members[i];
        Constant constantWithName = vm->constantPool[constantPoolIndex];
        assert(constantWithName.tag == STRING_CONST);

        BcField member;
        member.name = constantWithName.string;

        if(global){
            member.value = allocate_null(vm->heap);
        }else{
//            RuntimeObject * ptrValue = parentAndMemberValues[i];
            member.value = parentAndMemberValues[i + 1];
        }

        runtimeObject->fields[i] = member;
    }

    if(global){
        runtimeObject->parent = allocate_null(vm->heap);
    }else{
        runtimeObject->parent = parentAndMemberValues[0];
    }

    return runtimeObject;

}

void executeFetchDecodeExecuteLoop(VM * vm){

    bool endLoop = false;

    while(!endLoop){
        OpCode opCode = fetchByte(vm);

        switch (opCode) {
            case CONSTANT_OP:{
                size_t indexInConstPool = fetchNumber(2, vm);
                Constant constant = vm->constantPool[indexInConstPool];

                Pointer constantOnHeap = NULL;
                switch (constant.tag) {
                    case INTEGER_CONST:
                        constantOnHeap = allocate_integer(constant.integer, vm->heap);
                        break;
                    case NULL_CONST:
                        constantOnHeap = allocate_null(vm->heap);
                        break;
                    case FUNCTION_CONST:
                        constantOnHeap = allocate_bc_function(indexInConstPool, vm->heap);
                        break;
                    case BOOLEAN_CONST:
                        constantOnHeap = allocate_boolean(constant.boolean, vm->heap);
                        break;
                    default:
                        printf("UNKNOWN CONSTANT TYPE IN CONSTANT OPERATION\n");
                        exit(1);
                }

                operandStack_push(constantOnHeap, vm->operandStack);

                break;
            }
            case DROP_OP:
                operandStack_pop(vm->operandStack);
                break;
            case PRINT_OP:{

                // read format
                size_t indexInConstantPool = fetchNumber(2, vm);
                Constant constant = vm->constantPool[indexInConstantPool];

                assert(constant.tag == STRING_CONST);
                Str format = constant.string;

                // Read number of arguments
                u8 numberOfArguments = fetchByte(vm);

                Pointer * arguments = operandStack_popN(numberOfArguments, vm->operandStack);

                // Execute print
                executePrint(format, arguments, numberOfArguments);

                // Push null
                operandStack_push(allocate_null(vm->heap), vm->operandStack);

                break;
            }
            case RETURN_OP:{

                if(vm->frameStack->top != NULL){
                    vm->currentInstruction = vm->frameStack->top->returnAddress;
                    frameStack_destroy_frame(vm->frameStack);
                }else{
                    // Destroy global frame
                }
                endLoop = true;
                break;
            }
            case GET_LOCAL_OP:{
                size_t frameIndex = fetchNumber(2, vm);
                Pointer local = getPtrFromFrame(vm->frameStack, frameIndex);
                operandStack_push(local, vm->operandStack);
                break;
            }
            case SET_LOCAL_OP:{
                size_t frameIndex = fetchNumber(2, vm);
                Pointer operand = operandStack_peek(vm->operandStack);

                setPtrToFrame(vm->frameStack, frameIndex, operand);
                break;
            }

            case GET_GLOBAL_OP:{
                size_t constantIndex = fetchNumber(2, vm);
                Constant constant = vm->constantPool[constantIndex];
                assert(constant.tag == STRING_CONST);

                BcField * field = findBcField(vm->globalClass, constant.string);
                operandStack_push(field->value, vm->operandStack);
                break;
            }
            case SET_GLOBAL_OP:{

                size_t constantPoolIndex = fetchNumber(2, vm);
                Constant constant = vm->constantPool[constantPoolIndex];
                assert(constant.tag == STRING_CONST);

                Pointer valueToSet = operandStack_peek(vm->operandStack);
                BcField * field = findBcField(vm->globalClass, constant.string);

                field->value = valueToSet;
                break;
            }
            case JUMP_OP:{

                // TODO check for sign
                i16 relativeOffset = (i16) fetchNumber(2, vm);
                vm->currentInstruction += relativeOffset;

                break;
            }
            case BRANCH_OP:{

                i16 relativeOffset = (i16) fetchNumber(2, vm);
                Pointer pointer = operandStack_pop(vm->operandStack);

                if(bcIsTruth(pointer)){
                    vm->currentInstruction += relativeOffset;
                }

                break;
            }
            case CALL_FUNCTION_OP:{

                u8 numberOfArguments = fetchByte(vm);

                Pointer * arguments = operandStack_popN(numberOfArguments, vm->operandStack);
                Pointer function = operandStack_pop(vm->operandStack);

                BcFunctionRuntimeValue * functionRuntimeValue = (BcFunctionRuntimeValue *) function;
                assert(functionRuntimeValue->type == TYPE_FUNCTION);

                // Put null as this before arguments

                Pointer * argumentsWithThis = (Pointer *) malloc((numberOfArguments + 1) * sizeof(Pointer));

                argumentsWithThis[0] = allocate_null(vm->heap);
                for(int i = 1; i < numberOfArguments + 1; ++i){
                    argumentsWithThis[i] = arguments[i-1];
                }

                callFunction(functionRuntimeValue->indexToConstPool, argumentsWithThis, numberOfArguments + 1, vm);

                free(argumentsWithThis);
                break;
            }
            case ARRAY_OP:{
                Pointer initialValue = operandStack_pop(vm->operandStack);
                Pointer sizePtr = operandStack_pop(vm->operandStack);

                assert(((RuntimeObject *)sizePtr)->type == TYPE_INTEGER);
                IntegerRuntimeObject * size = (IntegerRuntimeObject*) sizePtr;
                assert(size->value >= 0);

                BcArrayRuntimeObject * arrayRuntimeObject = allocate_bc_array(size->value, initialValue, vm->heap);

                operandStack_push((Pointer) arrayRuntimeObject, vm->operandStack);
                break;
            }
            case OBJECT_OP:{

                u16 indexInConstantPool = fetchNumber(2, vm);
                Constant constant = vm->constantPool[indexInConstantPool];

                assert(constant.tag == CLASS_CONST);
                BcObjectRuntimeObject * objectRuntimeObject = instantiateClass(constant.class, vm, false);

                operandStack_push((Pointer) objectRuntimeObject, vm->operandStack);

                break;
            }
            case GET_FIELD_OP:{

                u16 constantIndex = fetchNumber(2, vm);
                Constant constant = vm->constantPool[constantIndex];
                assert(constant.tag == STRING_CONST);

                Pointer objPtr = operandStack_pop(vm->operandStack);
                RuntimeObject * obj = (RuntimeObject *)objPtr;
                assert(obj->type == TYPE_OBJECT);

                BcObjectRuntimeObject * bcObjectRuntimeObject = (BcObjectRuntimeObject *) objPtr;
                BcField * field = findBcField(bcObjectRuntimeObject, constant.string);
                operandStack_push(field->value, vm->operandStack);
                break;
            }
            case SET_FIELD_OP:{
                u16 constantIndex = fetchNumber(2, vm);
                Constant constant = vm->constantPool[constantIndex];
                assert(constant.tag == STRING_CONST);

                Pointer valuePtr = operandStack_pop(vm->operandStack);

                Pointer objPtr = operandStack_pop(vm->operandStack);
                RuntimeObject * obj = (RuntimeObject *)objPtr;
                assert(obj->type == TYPE_OBJECT);

                BcObjectRuntimeObject * bcObjectRuntimeObject = (BcObjectRuntimeObject *) objPtr;
                BcField * field = findBcField(bcObjectRuntimeObject, constant.string);
                field->value = valuePtr;
                operandStack_push(valuePtr, vm->operandStack);
                break;
            }
            case CALL_METHOD_OP:{
                u16 constantIndex = fetchNumber(2, vm);
                Constant methodName = vm->constantPool[constantIndex];
                assert(methodName.tag == STRING_CONST);

                u8 numberOfArguments = fetchByte(vm);

                Pointer * recieverAndArguments = operandStack_popN(numberOfArguments, vm->operandStack);

                bcCallMethod(methodName.string, *recieverAndArguments,
                             numberOfArguments > 1 ? recieverAndArguments + 1 : NULL, numberOfArguments - 1, vm);
                break;
            }
            default:{
                printf("UNKNOWN OP_CODE\n");
                exit(1);
            }
        }
    }


}

void loadGlobals(VM * vm){
    size_t numberOfGlobals = fetchNumber(2, vm);

    ClassConst globalClass;
    globalClass.memberCount = numberOfGlobals;
    globalClass.members = (u16 *) malloc(numberOfGlobals * sizeof(u16));

    for(size_t i = 0; i < numberOfGlobals; ++i){
        globalClass.members[i] = fetchNumber(2, vm);
    }

    vm->globalClass = instantiateClass(globalClass, vm, true);

    free(globalClass.members);

}


void startProgram(VM * vm){
    Pointer null = allocate_null(vm->heap);
    callFunction(vm->entryPoint, &null, 1, vm);
}

void bcInterpret(Instruction bcProgram){

    // Create vm
    VM * vm = initialize_VM(bcProgram, 1000000000, 100000);

    // Load header
    readHeader(vm);

    // Load constants
    loadConstants(vm);

    // Load globals
    loadGlobals(vm);

    // Load entry point
    loadEntryPoint(vm);

    // Execute program
    startProgram(vm);

    // Destroy vm
    destroy_vm(vm);

}


