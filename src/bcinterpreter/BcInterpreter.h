//
// Created by jerror on 3/25/23.
//

#ifndef RUN_BCINTERPRETER_H
#define RUN_BCINTERPRETER_H

typedef const unsigned char * Instruction;
typedef unsigned char BcByte;

void bcInterpret(Instruction bcProgram);

#endif //RUN_BCINTERPRETER_H
