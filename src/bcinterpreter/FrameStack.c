//
// Created by jerror on 3/25/23.
//

#include <malloc.h>
#include <stdlib.h>
#include <assert.h>
#include "FrameStack.h"

FrameStack * frameStack_alloc(){

    FrameStack * newStack = (FrameStack *) malloc(sizeof(FrameStack));
    newStack->top = NULL;

    return newStack;
}

Locals createLocals(size_t numberOfLocals){
    Locals locals;

    locals.locals = (Pointer *) malloc(numberOfLocals * sizeof(Pointer));
    locals.local_count = numberOfLocals;

    return locals;
}

Frame * frame_alloc(){
    return (Frame *) malloc(sizeof(Frame));
}
void frame_destroy(Frame * frame){
    free(frame->locals.locals);
    free(frame);
}

void frameStack_destroy(FrameStack * stack){

    // Destroy stack nodes
    Frame * currentFrame = stack->top;
    while(currentFrame != NULL){
        Frame * nextNode = currentFrame->previous;
        frame_destroy(currentFrame);
        currentFrame = nextNode;
    }

    // Destroy stack
    free(stack);
}

bool isGlobalFrame(Frame * frame){
    return frame->previous == NULL;
}


void frameStack_create_frame(FrameStack * stack, Instruction returnAddress, size_t numberOfLocals){
    Frame * frame = frame_alloc();

    // Setup stack node
    frame->previous = stack->top;
    frame->returnAddress = returnAddress;
    frame->locals = createLocals(numberOfLocals);

    // Set new top of the stack
    stack->top = frame;
}


void frameStack_destroy_frame(FrameStack * stack){

    // Get top
    Frame * currentTop = stack->top;

    if(currentTop == NULL){
        printf("FAIL: CANNOT POP EMPTY STACK");
        exit(1);
    }

    // Set new top
    stack->top = currentTop->previous;

    // Destroy old stackNode
    frame_destroy(currentTop);

}

Pointer getPtrFromFrame(FrameStack * stack, size_t index){
    assert(stack->top->locals.local_count > index);
    return stack->top->locals.locals[index];
}

void setPtrToFrame(FrameStack * stack, size_t index, Pointer value){
    stack->top->locals.locals[index] = value;
}

void addLocalToFrame(size_t localIndex, Pointer value, FrameStack * stack){
    stack->top->locals.locals[localIndex] = value;
}

void setReturnAddress(Instruction returnAddress, FrameStack * stack){
    stack->top->returnAddress = returnAddress;
}