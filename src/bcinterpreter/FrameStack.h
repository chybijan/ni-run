//
// Created by jerror on 3/25/23.
//

#ifndef RUN_FRAMESTACK_H
#define RUN_FRAMESTACK_H

#include "BcInterpreter.h"
#include "src/Heap.h"


typedef struct Locals{

    Pointer* locals;
    size_t local_count;

} Locals;

typedef struct Frame{
    struct Frame * previous;

    Instruction returnAddress;

    Locals locals;
} Frame;

typedef struct FrameStack{
    Frame * top;
} FrameStack;

FrameStack * frameStack_alloc();
void frameStack_destroy(FrameStack * stack);

Frame * frame_alloc();
void frame_destroy(Frame * stackNode);

void frameStack_create_frame(FrameStack * stack, Instruction returnAddress, size_t numberOfLocals);
void frameStack_destroy_frame(FrameStack * stack);

Pointer getPtrFromFrame(FrameStack * stack, size_t index);
void setPtrToFrame(FrameStack * stack, size_t index, Pointer value);

bool isGlobalFrame(Frame * frame);

void addLocalToFrame(size_t localIndex, Pointer value, FrameStack * stack);

void setReturnAddress(Instruction returnAddress, FrameStack * stack);

#endif //RUN_FRAMESTACK_H
