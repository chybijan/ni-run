//
// Created by jerror on 3/25/23.
//

#include <malloc.h>
#include <stdlib.h>
#include "OperandStack.h"

OperandStack * operandStack_alloc(size_t stackSize){
    OperandStack * newStack = (OperandStack *) malloc( sizeof(OperandStack));
    newStack->operands = (Pointer *) malloc(stackSize * sizeof(Pointer));
    newStack->stackSize = stackSize;
    newStack->topIndex = -1;

    return newStack;
}

void operandStack_destroy(struct OperandStack * stack){

    free(stack->operands);
    free(stack);
}

void operandStack_push(Pointer operand, OperandStack * stack){

    if(stack->topIndex > stack->stackSize && stack->topIndex != (size_t) -1){
        printf("Operand stack is too big\n");
        exit(1);
    }

    stack->topIndex += 1;

    stack->operands[stack->topIndex] = operand;
}
Pointer operandStack_pop(OperandStack * stack){

    Pointer popped = operandStack_peek(stack);
    stack->topIndex -= 1;

    return popped;
}

Pointer * operandStack_popN(size_t popAmount, OperandStack * stack){
    Pointer * popped = operandStack_peekN(popAmount, stack);

    stack->topIndex -= popAmount;

    return popped;
}

Pointer operandStack_peek(OperandStack * stack){
    if(stack->topIndex > stack->stackSize){
        printf("Cannot pop from empty stack\n");
        exit(1);
    }

    Pointer peeked = stack->operands[stack->topIndex];
    return peeked;
}

Pointer * operandStack_peekN(size_t peekAmount, OperandStack * stack){
    if(peekAmount == 0)
        return NULL;

    if(stack->topIndex < peekAmount-1){
        printf("Cannot pop from empty stack\n");
        exit(1);
    }

    return stack->operands + (stack->topIndex - (peekAmount-1));
}
