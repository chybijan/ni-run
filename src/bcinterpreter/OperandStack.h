//
// Created by jerror on 3/25/23.
//

#ifndef RUN_OPERANDSTACK_H
#define RUN_OPERANDSTACK_H

#include "FrameStack.h"

typedef struct OperandStack{

    Pointer * operands;

    size_t topIndex;
    size_t stackSize;
} OperandStack;

OperandStack * operandStack_alloc(size_t stackSize);
void operandStack_destroy(OperandStack * stack);

void operandStack_push(Pointer operand, OperandStack * stack);

Pointer operandStack_pop(OperandStack * stack);
Pointer * operandStack_popN(size_t popAmount, OperandStack * stack);

Pointer operandStack_peek(OperandStack * stack);
Pointer * operandStack_peekN(size_t peekAmount, OperandStack * stack);


#endif //RUN_OPERANDSTACK_H
