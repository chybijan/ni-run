#pragma once

// Can't include "dasm_proto.h" here, since that would not see Dst anad Dst_DECL
// definitions from "jit.h" and use default ones. And "jit.h" can't be included
// here, since it requires "mystate.h".
typedef struct dasm_State dasm_State;

typedef struct {
	dasm_State *ds;
	void **global_labels;
} MyState;
